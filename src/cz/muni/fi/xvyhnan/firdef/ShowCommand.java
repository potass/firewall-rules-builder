/**
 *
 */
package cz.muni.fi.xvyhnan.firdef;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.firdef.fragments.CommandFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.ConnectbotFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.ExtendedACLFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.StandardACLFragment;

/**
 * Show to user the generated command and implements ConnectBot.
 * @author Jan Vyhn�nek
 */
public class ShowCommand extends TabSwipeActivity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.command2));

        addTab("COMMAND", CommandFragment.class, StandardACLFragment.createBundle("COMMAND"));
        addTab("CONNECTBOT", ConnectbotFragment.class, ExtendedACLFragment.createBundle("CONNECTBOT"));
    }

	@Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
