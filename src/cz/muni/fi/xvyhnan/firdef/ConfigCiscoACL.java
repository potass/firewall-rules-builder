/**
 *
 */
package cz.muni.fi.xvyhnan.firdef;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.firdef.fragments.ExtendedACLFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.StandardACLFragment;

/**
 * Shows configuration window of access lists.
 * @author Jan Vyhn�nek
 */
public class ConfigCiscoACL extends TabSwipeActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.activityciscotitle));

        addTab("STANDARD ACL", StandardACLFragment.class, StandardACLFragment.createBundle("STANDARD ACL"));
        addTab("EXTENDED ACL", ExtendedACLFragment.class, ExtendedACLFragment.createBundle("EXTENDED ACL"));
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuItem help = menu.add("Help");
    	help.setIcon(android.R.drawable.ic_menu_help);
    	help.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    	help.setIntent(new Intent(this, Help.class));
		return true;
	}

    @Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}


