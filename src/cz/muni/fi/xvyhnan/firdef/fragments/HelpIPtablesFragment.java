/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cz.muni.fi.xvyhnan.firdef.R;

/**
 * Fragment showing iptables section of help.
 * @author Jan Vyhn�nek
 */
public class HelpIPtablesFragment extends Fragment {
	public static final String EXTRA_TITLE = "IPTABLES";

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

    	View v = new View(getActivity());
        v = inflater.inflate(R.layout.helpiptables, container, false);
        return v;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    }

    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }
}
