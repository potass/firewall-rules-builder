/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.fragments;

import java.util.HashSet;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import cz.muni.fi.xvyhnan.firdef.IConfiguration;
import cz.muni.fi.xvyhnan.firdef.InputDataValidation;
import cz.muni.fi.xvyhnan.firdef.R;
import cz.muni.fi.xvyhnan.firdef.ShowCommand;
import cz.muni.fi.xvyhnan.firdef.validation.ExtendedACLNumberValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv4Validation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6MaskValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6Validation;
import cz.muni.fi.xvyhnan.firdef.validation.PortValidation;

/**
 * Fragment showing configuration of extended ACL.
 * @author Jan Vyhn�nek
 */
public class ExtendedACLFragment extends Fragment implements IConfiguration {
	public static final String EXTRA_TITLE = "EXTENDED ACL";
	private final String ERROR = "!!!";
	private final int DEFAULT_TEXT_COLOR = Color.WHITE;
	private final int DEFAULT_TEXT_COLOR_WHITE = Color.BLACK;
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private final boolean DEFAULT_VALIDATION = true;
	private InputDataValidation validator;
	private IPv6Validation ipv6Validation;
	private int selectedMatchingCondition = 0;
	private View layout;
	private Spinner matchingCondsExt, protocols;
	private RadioGroup aclDenyPermitExt, aclIPv4IPv6Ext;
	private RadioButton ipv4Ext, ipv6Ext;
	private CheckBox aclLogExtChB, aclRangeExtChB, aclAnySourceExtChB, aclAnyDestExtChB,
			aclAnySourceIPv6ExtChB, aclAnyDestIPv6ExtChB, otherProtocolChB, aclNamedTypeChB;
	private TextView aclPointExt1, aclPointExt2, aclPointExt3, aclPointExt4, aclPointExt5, aclPointExt6, aclPointExt7,
			aclPointExt8, aclPointExt9, aclPointExt10,	aclPointExt11, aclPointExt12, aclMatchingCondExt,
			aclTcpAppExt, doublePointExt1, doublePointExt2, doublePointExt3, doublePointExt4, doublePointExt5,
			doublePointExt6, doublePointExt7, doublePointExt8, doublePointExt9, doublePointExt10,
			doublePointExt11, doublePointExt12, doublePointExt13, doublePointExt14, aclAnySourceExt,
			aclAnyDestExt, aclSourceIPExt, aclDestIPExt;
	private EditText aclNumberExtET, aclOtherProtocolET,
			aclSourceIPExtET1, aclSourceIPExtET2, aclSourceIPExtET3, aclSourceIPExtET4, aclDestinationIPExtET1,
			aclDestinationIPExtET2, aclDestinationIPExtET3, aclDestinationIPExtET4, aclWildcardExtET1,
			aclWildcardExtET2, aclWildcardExtET3, aclWildcardExtET4, aclWildcardExt2ET1, aclWildcardExt2ET2,
			aclWildcardExt2ET3, aclWildcardExt2ET4, aclFromExtET, aclToExtET, aclTcpAppExtET, aclNameET,
			sourceIPv6ExtET1, sourceIPv6ExtET2, sourceIPv6ExtET3, sourceIPv6ExtET4, sourceIPv6ExtET5,
			sourceIPv6ExtET6, sourceIPv6ExtET7, sourceIPv6ExtET8, destinationIPv6ExtET1, destinationIPv6ExtET2,
			destinationIPv6ExtET3, destinationIPv6ExtET4, destinationIPv6ExtET5, destinationIPv6ExtET6,
			destinationIPv6ExtET7, destinationIPv6ExtET8, sourceMaskIPv6ExtET, destinationMaskIPv6ExtET;
	private View[] sourceIPv4ExtControls, destIPv4ExtControls, sourceIPv6ExtControls, destIPv6ExtControls;
	private RelativeLayout sourceIPv4ExtRL, destinationIPv4ExtRL, sourceIPv6ExtRL, destinationIPv6ExtRL,
			sourceWildcardExtRL, destinationWildcardExtRL, sourceMaskExtRL, destinationMaskExtRL,
			destinationIPHeader, matchingConditionHeader;
	private LayoutParams aclMatchingCondExtP, destinationIPExtP;
	private EditText[] sourceIPv6ExtControlsET, destinationIPv6ExtControlsET, sourceIPv4ExtControlsET,
			destinationIPv4ExtControlsET, sourceWildcardExtControlsET, destinationWildcardExtControlsET;
	private ImageButton aclNumberErase, aclProtocolErase, aclSourceIPErase, aclDestinationIPErase,
			aclMatchingConditionErase, aclTcpAppErase, aclNameErase;
	private Button generate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View v = new View(getActivity());
        v = inflater.inflate(R.layout.extendedacl, container, false);
        layout = v;
        return v;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

    	addListenerOnMatchingConditionsItemSelection();
        initialization();
    }

    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }

    @Override
    public void onResume() {
    	super.onResume();

		for(EditText e : sourceIPv4ExtControlsET) {
			setError(e, false);
		}
		for(EditText e : destinationIPv4ExtControlsET) {
			setError(e, false);
		}
		for(EditText e : sourceIPv6ExtControlsET) {
			setError(e, false);
		}
		for(EditText e : destinationIPv6ExtControlsET) {
			setError(e, false);
		}
		for(EditText e : sourceWildcardExtControlsET) {
			setError(e, false);
		}
		for(EditText e : destinationWildcardExtControlsET) {
			setError(e, false);
		}

		setError(aclNumberExtET, false);
		setError(aclOtherProtocolET, false);
		setError(sourceMaskIPv6ExtET, false);
		setError(destinationMaskIPv6ExtET, false);
		setError(aclFromExtET, false);
		setError(aclToExtET, false);
		setError(aclNameET, false);
    }

    /**
     * Adds listener on the matching condition selection spinner.
     */
    private void addListenerOnMatchingConditionsItemSelection() {
    	matchingCondsExt = (Spinner) layout.findViewById(R.id.matchingcondsext);
    	matchingCondsExt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()	{
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	selectedMatchingCondition = position;
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedMatchingCondition = 0;
    	    }
    	});
    }

    public void onIPSelectionClick(View v) {
    	int checkedIP = aclIPv4IPv6Ext.getCheckedRadioButtonId();
    	if(checkedIP == R.id.aclipv4ext) {
    		aclMatchingCondExtP.addRule(RelativeLayout.BELOW, R.id.aclanydestextChB);
    		destinationIPExtP.addRule(RelativeLayout.BELOW, R.id.aclanysourceextChB);

    		setLayoutBelow(matchingConditionHeader, aclMatchingCondExtP);
    		setLayoutBelow(destinationIPHeader, destinationIPExtP);

    		aclSourceIPExt.setText(R.string.sourceipwildcard);
    		aclDestIPExt.setText(R.string.destinationipwildcard);

			setVisibility(sourceIPv4ExtRL, 0);
			setVisibility(destinationIPv4ExtRL, 0);
			setVisibility(sourceWildcardExtRL, 0);
    		setVisibility(destinationWildcardExtRL, 0);
    		setVisibility(aclAnySourceExtChB, 0);
    		setVisibility(aclAnySourceExt, 0);
    		setVisibility(aclAnyDestExtChB, 0);
    		setVisibility(aclAnyDestExt, 0);
			setVisibility(sourceIPv6ExtRL, 4);
			setVisibility(destinationIPv6ExtRL, 4);
			setVisibility(sourceMaskExtRL, 4);
			setVisibility(destinationMaskExtRL, 4);
    	} else {
    		aclMatchingCondExtP.addRule(RelativeLayout.BELOW, R.id.acldestinationmaskextRL);
    		destinationIPExtP.addRule(RelativeLayout.BELOW, R.id.aclsourcemaskextRL);

    		setLayoutBelow(matchingConditionHeader, aclMatchingCondExtP);
    		setLayoutBelow(destinationIPHeader, destinationIPExtP);

    		aclSourceIPExt.setText(R.string.sourceipmask);
    		aclDestIPExt.setText(R.string.destinationipmask);

    		setVisibility(sourceIPv4ExtRL, 4);
    		setVisibility(destinationIPv4ExtRL, 4);
    		setVisibility(sourceWildcardExtRL, 4);
    		setVisibility(destinationWildcardExtRL, 4);
    		setVisibility(aclAnySourceExtChB, 4);
    		setVisibility(aclAnySourceExt, 4);
    		setVisibility(aclAnyDestExtChB, 4);
    		setVisibility(aclAnyDestExt, 4);
    		setVisibility(sourceIPv6ExtRL, 0);
			setVisibility(destinationIPv6ExtRL, 0);
			setVisibility(sourceMaskExtRL, 0);
			setVisibility(destinationMaskExtRL, 0);
    	}
    }

    /**
     * Validates inserted values, generates the filtering rule and shows it, considering the extended access lists.
     * @param v Pressed button.
     */
    public void onExtendedGenerateClick(View v) {
    	boolean error = false;

    	Set<String> alert = new HashSet<String>();

    	Set<EditText> errorControls = new HashSet<EditText>();

    	String[] ipOctetsSource = new String[]{aclSourceIPExtET1.getText().toString(), aclSourceIPExtET2.getText().toString(),
    			aclSourceIPExtET3.getText().toString(), aclSourceIPExtET4.getText().toString()};
    	String[] maskOctetsDest = new String[]{aclWildcardExtET1.getText().toString(), aclWildcardExtET2.getText().toString(),
    			aclWildcardExtET3.getText().toString(), aclWildcardExtET4.getText().toString()};
    	String[] ipOctetsDest = new String[]{aclDestinationIPExtET1.getText().toString(), aclDestinationIPExtET2.getText().toString(),
    			aclDestinationIPExtET3.getText().toString(), aclDestinationIPExtET4.getText().toString()};
    	String[] maskOctetsSource = new String[]{aclWildcardExt2ET1.getText().toString(), aclWildcardExt2ET2.getText().toString(),
    			aclWildcardExt2ET3.getText().toString(), aclWildcardExt2ET4.getText().toString()};
    	String[] sourceIPv6Address = new String[]{sourceIPv6ExtET1.getText().toString(),
				sourceIPv6ExtET2.getText().toString(), sourceIPv6ExtET3.getText().toString(),
				sourceIPv6ExtET4.getText().toString(), sourceIPv6ExtET5.getText().toString(),
				sourceIPv6ExtET6.getText().toString(), sourceIPv6ExtET7.getText().toString(),
				sourceIPv6ExtET8.getText().toString()};
		String[] destinationIPv6Address = new String[]{destinationIPv6ExtET1.getText().toString(),
				destinationIPv6ExtET2.getText().toString(), destinationIPv6ExtET3.getText().toString(),
				destinationIPv6ExtET4.getText().toString(), destinationIPv6ExtET5.getText().toString(),
				destinationIPv6ExtET6.getText().toString(), destinationIPv6ExtET7.getText().toString(),
				destinationIPv6ExtET8.getText().toString()};

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    	if(colorPreferences.getBoolean("validation", DEFAULT_VALIDATION)) {
			ipv6Validation.setError(true);

			for (EditText e : sourceIPv4ExtControlsET) {
				setError(e, false);
			}
			for (EditText e : destinationIPv4ExtControlsET) {
				setError(e, false);
			}
			for (EditText e : sourceIPv6ExtControlsET) {
				setError(e, false);
			}
			for (EditText e : destinationIPv6ExtControlsET) {
				setError(e, false);
			}
			for (EditText e : sourceWildcardExtControlsET) {
				setError(e, false);
			}
			for (EditText e : destinationWildcardExtControlsET) {
				setError(e, false);
			}

			setError(aclNumberExtET, false);
			setError(aclOtherProtocolET, false);
			setError(sourceMaskIPv6ExtET, false);
			setError(destinationMaskIPv6ExtET, false);
			setError(aclNameET, false);

			if (aclNamedTypeChB.isChecked() && (aclNameET.getText().toString().replaceAll(" ", "").equals("")
					|| !isAcceptedValue(aclNameET.getText().toString()))) {
				setError(aclNameET, true);
				alert.add(getString(R.string.missingaclname) + "\n\n");
				error = true;
			}

			if (!aclNamedTypeChB.isChecked()
					&& !validator.isExtendedAclNumber(aclNumberExtET)) {
				alert.add(getString(R.string.wrongaclnumberext) + "\n\n");
				error = true;
			}

			if (otherProtocolChB.isChecked()
					&& (aclOtherProtocolET.getText().toString()
							.replaceAll(" ", "").equals("") || !isAcceptedValue(aclOtherProtocolET
							.getText().toString()))) {
				alert.add(getString(R.string.missingotherprotocol) + "\n\n");
				setError(aclOtherProtocolET, true);
				errorControls.add(aclOtherProtocolET);
				error = true;
			}

			if (!aclNamedTypeChB.isChecked()
					|| (aclNamedTypeChB.isChecked() && aclIPv4IPv6Ext
							.getCheckedRadioButtonId() == R.id.aclipv4ext)) {
				if (!aclAnySourceExtChB.isChecked()
						&& !validator.isIpAddress(sourceIPv4ExtControlsET)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}
				if (!aclAnySourceExtChB.isChecked()
						&& !validator.isWildcard(sourceWildcardExtControlsET)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}
				if (!aclAnyDestExtChB.isChecked()
						&& !validator.isIpAddress(destinationIPv4ExtControlsET)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}
				if (!aclAnyDestExtChB.isChecked()
						&& !validator
								.isWildcard(destinationWildcardExtControlsET)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}
			} else if (aclNamedTypeChB.isChecked()
					&& aclIPv4IPv6Ext.getCheckedRadioButtonId() == R.id.aclipv6ext) {
				if (!aclAnySourceIPv6ExtChB.isChecked()
						&& (!validator.isIPv6Address(sourceIPv6ExtControlsET) || isIPv6Empty(sourceIPv6ExtControlsET))) {
					alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
					error = true;
				}
				if (!aclAnyDestIPv6ExtChB.isChecked()
						&& (!validator
								.isIPv6Address(destinationIPv6ExtControlsET) || isIPv6Empty(destinationIPv6ExtControlsET))) {
					alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
					error = true;
				}

				if (!aclAnySourceIPv6ExtChB.isChecked()
						&& !sourceMaskIPv6ExtET.getText().toString()
								.replaceAll(" ", "").equals("")
						&& !validator.isIPv6MaskCorrect(sourceMaskIPv6ExtET)) {
					alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
					error = true;
				}

				if (!aclAnySourceIPv6ExtChB.isChecked()
						&& !sourceMaskIPv6ExtET.getText().toString()
								.replaceAll(" ", "").equals("")
						&& isIPv6Empty(sourceIPv6ExtControlsET)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}

				if (!aclAnyDestIPv6ExtChB.isChecked()
						&& !destinationMaskIPv6ExtET.getText().toString()
								.replaceAll(" ", "").equals("")
						&& !validator
								.isIPv6MaskCorrect(destinationMaskIPv6ExtET)) {
					alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
					error = true;
				}

				if (!aclAnyDestIPv6ExtChB.isChecked()
						&& !destinationMaskIPv6ExtET.getText().toString()
								.replaceAll(" ", "").equals("")
						&& isIPv6Empty(destinationIPv6ExtControlsET)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}
			}

			aclFromExtET.getBackground().clearColorFilter();
			aclToExtET.getBackground().clearColorFilter();

			setError(aclFromExtET, false);
			setError(aclToExtET, false);

			if (aclRangeExtChB.isChecked()
					&& !aclFromExtET.getText().toString().replaceAll(" ", "")
							.equals("")
					&& !aclToExtET.getText().toString().replaceAll(" ", "")
							.equals("")
					&& !validator.isToFromCorrect(aclFromExtET, aclToExtET)) {
				alert.add(getString(R.string.wrongrange) + "\n\n");
				error = true;
			}

			if ((!aclFromExtET.getText().toString().replaceAll(" ", "")
					.equals("") && aclToExtET.getText().toString()
					.replaceAll(" ", "").equals(""))
					|| (aclFromExtET.getText().toString().replaceAll(" ", "")
							.equals("") && !aclToExtET.getText().toString()
							.replaceAll(" ", "").equals(""))) {
				alert.add(getString(R.string.missingrange) + "\n\n");
				if ((!aclFromExtET.getText().toString().replaceAll(" ", "")
						.equals("") && aclToExtET.getText().toString()
						.replaceAll(" ", "").equals(""))) {
					setError(aclToExtET, true);
					errorControls.add(aclToExtET);
				} else if ((aclFromExtET.getText().toString()
						.replaceAll(" ", "").equals("") && !aclToExtET
						.getText().toString().replaceAll(" ", "").equals(""))) {
					setError(aclFromExtET, true);
					errorControls.add(aclToExtET);
				}
				error = true;
			}

			ipv6Validation.setError(false);
		}

    	if(error) {
    		makeAlertDialog(alert);
    	} else {
    		String command = "";
    		String preCommand = "";
    		if(aclNamedTypeChB.isChecked()) {
    			command = denyOrPermit(2) +" ";

    			if(aclIPv4IPv6Ext.getCheckedRadioButtonId() == R.id.aclipv4ext) {
    				preCommand = "ip access-list extended "+ aclNameET.getText().toString();
    			} else {
    				preCommand = "ipv6 access-list "+ aclNameET.getText().toString();
    			}
    		} else {
    			command = "access-list "+ aclNumberExtET.getText().toString() +" "+ denyOrPermit(2) +" ";
    		}

    		if(otherProtocolChB.isChecked()) {
    			command += aclOtherProtocolET.getText().toString() +" ";
    		} else {
    			command += protocols.getSelectedItem().toString() + " ";
    		}

			if(aclIPv4IPv6Ext.getCheckedRadioButtonId() == R.id.aclipv4ext) {
				command += (aclAnySourceExtChB.isChecked() ? "any " : makeAddress(ipOctetsSource) +" "+ makeAddress(maskOctetsSource) +" ");
				command += (aclAnyDestExtChB.isChecked() ? "any " : makeAddress(ipOctetsDest) +" "+ makeAddress(maskOctetsDest) +" ");
			} else {
				command += (aclAnySourceIPv6ExtChB.isChecked() ? "any " : makeIPv6Address(sourceIPv6Address));
				if(!aclAnySourceIPv6ExtChB.isChecked() && !sourceMaskIPv6ExtET.getText().toString().replaceAll(" ", "").equals("")) {
					command += "/"+ removeZeros(sourceMaskIPv6ExtET.getText().toString());
				}
				if(!aclAnySourceIPv6ExtChB.isChecked()) {
					command += " ";
				}

				command += (aclAnyDestIPv6ExtChB.isChecked() ? "any " : makeIPv6Address(destinationIPv6Address));
				if(!aclAnyDestIPv6ExtChB.isChecked() && !destinationMaskIPv6ExtET.getText().toString().replaceAll(" ", "").equals("")) {
					command += "/"+ removeZeros(destinationMaskIPv6ExtET.getText().toString());
				}
				if(!aclAnyDestIPv6ExtChB.isChecked()) {
					command += " ";
				}
			}

			if(!aclRangeExtChB.isChecked() && !aclTcpAppExtET.getText().toString().replaceAll(" ", "").equals("")) {
				if(selectedMatchingCondition == 1) {
					command += "eq ";
				} else if(selectedMatchingCondition == 2) {
					command += "neq ";
				} else if(selectedMatchingCondition == 3) {
					command += "gt ";
				} else if(selectedMatchingCondition == 4) {
					command += "lt ";
				} else {}

				command += (!aclTcpAppExtET.getText().toString().replaceAll(" ", "").equals("") ? aclTcpAppExtET.getText().toString() +" " : "");
			} else if(aclRangeExtChB.isChecked()) {
				if(!aclFromExtET.getText().toString().replaceAll(" ", "").equals("") && !aclToExtET.getText().toString().replaceAll(" ", "").equals("")) {
					command += "range "+ removeZeros(aclFromExtET.getText().toString()) +" "
							+ removeZeros(aclToExtET.getText().toString())+ " ";
				}
			}

    		command += (isLogChecked(2) ? "log" : "");

    		Intent intent = new Intent(getActivity(), ShowCommand.class);
    		intent.putExtra("command", command);
    		if(aclNamedTypeChB.isChecked()) {
    			intent.putExtra("precommand", preCommand);
    		}
    		startActivity(intent);
    	}
    }

    public void onOtherProtocolClick(View v) {
    	if(otherProtocolChB.isChecked()) {
    		setEnabledAndFocusable(aclOtherProtocolET, true);
    		setEnabledAndFocusable(protocols, false);
    	} else {
    		setEnabledAndFocusable(aclOtherProtocolET, false);
    		setEnabledAndFocusable(protocols, true);
    	}
    }

    /**
     * Enables to insert the range of accepted ports.
     * @param v Pressed check box.
     */
    public void onRangeClick(View v) {
    	if(aclRangeExtChB.isChecked()) {
    		setEnabledAndFocusable(aclMatchingCondExt, false);
    		setEnabledAndFocusable(matchingCondsExt, false);
    		setEnabledAndFocusable(aclFromExtET, true);
    		setEnabledAndFocusable(aclToExtET, true);
    		setEnabledAndFocusable(aclTcpAppExt, false);
    		setEnabledAndFocusable(aclTcpAppExtET, false);
    	} else {
    		setEnabledAndFocusable(aclMatchingCondExt, true);
    		setEnabledAndFocusable(matchingCondsExt, true);
    		setEnabledAndFocusable(aclFromExtET, false);
    		setEnabledAndFocusable(aclToExtET, false);
    		setEnabledAndFocusable(aclTcpAppExt, true);
    		setEnabledAndFocusable(aclTcpAppExtET, true);
    	}
    }

    /**
     * Enables to accept all IP addresses.
     * @param v Pressed check box.
     */
    public void onAnyClick(View v) {
    	switch (v.getId()) {
	  		case R.id.aclanysourceextChB:
	  			if(aclAnySourceExtChB.isChecked()) {
					setEnabledAndFocusable(sourceIPv4ExtControls, false);
				} else {
					setEnabledAndFocusable(sourceIPv4ExtControls, true);
				}
	  			break;
	  		case R.id.aclanydestextChB:
	  			if(aclAnyDestExtChB.isChecked()) {
					setEnabledAndFocusable(destIPv4ExtControls, false);
				} else {
					setEnabledAndFocusable(destIPv4ExtControls, true);
				}
	  			break;
	  		case R.id.aclanysourceipv6extChB:
	  			if(aclAnySourceIPv6ExtChB.isChecked()) {
	  				setEnabledAndFocusable(sourceIPv6ExtControls, false);
	  				setEnabledAndFocusable(sourceMaskIPv6ExtET, false);
				} else {
					setEnabledAndFocusable(sourceIPv6ExtControls, true);
					setEnabledAndFocusable(sourceMaskIPv6ExtET, true);
				}
	  			break;
	  		case R.id.aclanydestipv6extChB:
	  			if(aclAnyDestIPv6ExtChB.isChecked()) {
	  				setEnabledAndFocusable(destIPv6ExtControls, false);
	  				setEnabledAndFocusable(destinationMaskIPv6ExtET, false);
				} else {
					setEnabledAndFocusable(destIPv6ExtControls, true);
					setEnabledAndFocusable(destinationMaskIPv6ExtET, true);
				}
	  			break;
    	}
    }

    /**
     * Erases the content of specific EditText(s).
     * @param v Pressed button.
     */
    public void onEraseClick(View v) {
    	switch (v.getId()) {
			case R.id.aclnumberexterase:
				eraseInsertedData(aclNumberExtET);
				break;
			case R.id.aclprotocolexterase:
				eraseInsertedData(aclOtherProtocolET);
				break;
			case R.id.aclsourceipexterase:
				if(ipv4Ext.isChecked()) {
					for (EditText et : sourceIPv4ExtControlsET) {
						eraseInsertedData(et);
					}
					for (EditText et : sourceWildcardExtControlsET) {
						eraseInsertedData(et);
					}
				} else {
					for (EditText et : sourceIPv6ExtControlsET) {
						eraseInsertedData(et);
					}
					eraseInsertedData(sourceMaskIPv6ExtET);
				}
				break;
			case R.id.acldestinationipexterase:
				if(ipv4Ext.isChecked()) {
					for (EditText et : destinationIPv4ExtControlsET) {
						eraseInsertedData(et);
					}
					for (EditText et : destinationWildcardExtControlsET) {
						eraseInsertedData(et);
					}
				} else {
					for (EditText et : destinationIPv6ExtControlsET) {
						eraseInsertedData(et);
					}
					eraseInsertedData(destinationMaskIPv6ExtET);
				}
				break;
			case R.id.aclmatchingcondexterase:
				eraseInsertedData(aclFromExtET);
				eraseInsertedData(aclToExtET);
				break;
			case R.id.acltcpappexterase:
				eraseInsertedData(aclTcpAppExtET);
				break;
			case R.id.aclnameexterase:
				eraseInsertedData(aclNameET);
				break;
    	}
    }

    public void eraseInsertedData(EditText et) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
    	} else {
    		et.setTextColor(DEFAULT_TEXT_COLOR);
    	}
		et.setText("");
    }

    /**
     * Enables or disables inserting of ACL name.
     * @param v Pressed checkbox.
     */
    public void onNamedTypeClick(View v) {
    	CheckBox c = (CheckBox) v;
    	if(c.isChecked()) {
    		setEnabledAndFocusable(aclNameET, true);
    		setEnabledAndFocusable(aclNumberExtET, false);

	    	setEnabledAndFocusable(ipv4Ext, true);
			setEnabledAndFocusable(ipv6Ext, true);
			aclIPv4IPv6Ext.check(R.id.aclipv4ext);
			onIPSelectionClick(ipv4Ext);
    	} else {
    		setEnabledAndFocusable(aclNameET, false);
    		setEnabledAndFocusable(aclNumberExtET, true);

    		setEnabledAndFocusable(ipv4Ext, false);
			setEnabledAndFocusable(ipv6Ext, false);
			aclIPv4IPv6Ext.check(R.id.aclipv4ext);
			onIPSelectionClick(ipv4Ext);
    	}
    }

    public void setEnabledAndFocusable(View[] controls, boolean bool) {
    	for(View control : controls) {
    		setEnabledAndFocusable(control, bool);
		}
    }

    public void setEnabledAndFocusable(View control, boolean bool) {
		control.setEnabled(bool);
    	if(!(control instanceof Spinner) && !(control instanceof RadioButton)) {
    		control.setFocusable(bool);
			control.setFocusableInTouchMode(bool);
    	}
    }

    /**
     * Differentiates whether deny or permit is selected.
     * @param mode Type of access list.
     * @return Selected value.
     */
    private String denyOrPermit(int mode) {
    	String ret = "";
    	int checked = -1;

		checked = aclDenyPermitExt.getCheckedRadioButtonId();

    	switch (checked) {
    	  	case R.id.acldeny:
    	  		ret = "deny";
    	  		break;
    	  	case R.id.aclpermit:
    	  		ret = "permit";
    	  		break;
    	  	case R.id.acldenyext:
    	  		ret = "deny";
    	  		break;
    	  	case R.id.aclpermitext:
    	  		ret = "permit";
    	  		break;
    	}

    	return ret;
    }

    /**
     * Determines whether the log is checked.
     * @param mode Type of access list.
     * @return True if the log is selected, false otherwise.
     */
    private boolean isLogChecked(int mode) {
    	return aclLogExtChB.isChecked();
    }

    public String makeAddress(String[] octets) {
    	String ret = "";
    	for(int i = 0; i < octets.length; i++) {
    		while(octets[i].length() > 1 && octets[i].startsWith("0")) {
    			octets[i] = octets[i].substring(1);
    		}
    		ret += octets[i];
    		if(i != octets.length - 1) {
    			ret += ".";
    		}
    	}
    	return ret;
    }

    public String makeIPv6Address(String[] parts) {
    	for(int i = 0; i < parts.length; i++) {
    		if(parts[i].length() > 0) {
    			while(parts[i].length() > 1 && parts[i].startsWith("0")) {
    				parts[i] = parts[i].substring(1);
    			}
    		} else {
    			parts[i] = "0";
    		}
    	}

    	int begin = -1, end = -1;
    	for(int i = 0; i < parts.length - 1; i++) {
    		if(parts[i].equals("0") && parts[i + 1].equals("0")) {
    			if(begin == -1) {
    				begin = i;
    			}
    			end = i + 1;
    		} else if(i > 0 && parts[i - 1].equals("0") && parts[i].equals("0") && !parts[i + 1].equals("0")) {
    			break;
    		}
    	}

    	String ret = "";
    	for(int i = 0; i < parts.length; i++) {
    		if(i == begin || i == end) {
    			ret += ":";
    		} else if(i < begin || i > end) {
    			ret += parts[i].toUpperCase();
        		if(i != parts.length - 1) {
        			ret += ":";
        		}
    		}
    	}
    	ret = ret.replace(":::", "::");

    	return ret;
    }

    public String removeZeros(String number) {
    	while(number.length() > 1 && number.startsWith("0")) {
    		number = number.substring(1);
		}
    	return number;
    }

    public void makeAlertDialog(Set<String> alerts) {
    	String alert = "";
    	for(String s : alerts) {
    		alert += s;
    	}

    	ScrollView alertScroll = new ScrollView(getActivity().getApplicationContext());
    	TextView alertText = new TextView(getActivity().getApplicationContext());
    	alertText.setText(alert.subSequence(0, alert.length() - 2));
    	alertText.setTypeface(null, Typeface.BOLD);
    	alertText.setTextColor(Color.rgb(255, 172, 0));
    	alertText.setTextSize(16);
    	alertText.setGravity(17);
    	alertText.setPadding(4, 0, 4, 0);
    	alertScroll.addView(alertText);

    	AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
    	dialog.setIcon(android.R.drawable.ic_dialog_alert);
    	dialog.setTitle("Errors of inserted values");
    	dialog.setView(alertScroll);
    	dialog.setPositiveButton("OK", null);
    	dialog.show();
    }

    public void setVisibility(View control, int visibility) {
		control.setVisibility(visibility);
    }

    public void setLayoutBelow(View control, LayoutParams params) {
		control.setLayoutParams(params);
    }

    public boolean isIPEmpty(EditText[] octets) {
    	boolean ret = false;
    	for(EditText octet : octets) {
    		if(octet.getText().toString().replaceAll(" ", "").equals("")) {
				setError(octet, true);
				ret = true;
    		}
    	}
    	return ret;
    }

    public boolean isIPv6Empty(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	if(ret) {
    		for(EditText octet : octets) {
        		setError(octet, true);
    		}
    	}
    	return ret;
    }

    public boolean isIPEmptyWithoutErrorWarning(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	return ret;
    }

    public void setError(View control, boolean enabled) {
    	EditText et = (EditText)control;
    	if(enabled) {
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    		et.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
    		et.setTextColor(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)));
    		et.setText(ERROR);
    	} else {
    		et.getBackground().clearColorFilter();
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
        	} else {
        		et.setTextColor(DEFAULT_TEXT_COLOR);
        	}
    		if(et.getText().toString().equals(ERROR)) {
    			et.setText("");
    		}
    	}
    }

    public boolean isAcceptedValue(String value) {
		if(value.equals(ERROR)) return false;
		return true;
	}

    public void initialization() {
    	validator = new InputDataValidation(PreferenceManager.getDefaultSharedPreferences(getActivity()));

    	protocols = (Spinner) layout.findViewById(R.id.acltypeofprotocol);
        aclPointExt1 = (TextView) layout.findViewById(R.id.aclippointext1);
        aclPointExt2 = (TextView) layout.findViewById(R.id.aclippointext2);
        aclPointExt3 = (TextView) layout.findViewById(R.id.aclippointext3);
        aclPointExt4 = (TextView) layout.findViewById(R.id.aclippointext4);
        aclPointExt5 = (TextView) layout.findViewById(R.id.aclippointext5);
        aclPointExt6 = (TextView) layout.findViewById(R.id.aclippointext6);
        aclPointExt7 = (TextView) layout.findViewById(R.id.aclippointext7);
        aclPointExt8 = (TextView) layout.findViewById(R.id.aclippointext8);
        aclPointExt9 = (TextView) layout.findViewById(R.id.aclippointext9);
        aclPointExt10 = (TextView) layout.findViewById(R.id.aclippointext10);
        aclPointExt11 = (TextView) layout.findViewById(R.id.aclippointext11);
        aclPointExt12 = (TextView) layout.findViewById(R.id.aclippointext12);
        doublePointExt1 = (TextView) layout.findViewById(R.id.acldoublepointext1);
        doublePointExt2 = (TextView) layout.findViewById(R.id.acldoublepointext2);
        doublePointExt3 = (TextView) layout.findViewById(R.id.acldoublepointext3);
        doublePointExt4 = (TextView) layout.findViewById(R.id.acldoublepointext4);
        doublePointExt5 = (TextView) layout.findViewById(R.id.acldoublepointext5);
        doublePointExt6 = (TextView) layout.findViewById(R.id.acldoublepointext6);
        doublePointExt7 = (TextView) layout.findViewById(R.id.acldoublepointext7);
        doublePointExt8 = (TextView) layout.findViewById(R.id.acldoublepointext8);
        doublePointExt9 = (TextView) layout.findViewById(R.id.acldoublepointext9);
        doublePointExt10 = (TextView) layout.findViewById(R.id.acldoublepointext10);
        doublePointExt11 = (TextView) layout.findViewById(R.id.acldoublepointext11);
        doublePointExt12 = (TextView) layout.findViewById(R.id.acldoublepointext12);
        doublePointExt13 = (TextView) layout.findViewById(R.id.acldoublepointext13);
        doublePointExt14 = (TextView) layout.findViewById(R.id.acldoublepointext14);
        aclTcpAppExt = (TextView) layout.findViewById(R.id.acltcpappext);
        aclAnySourceExt = (TextView) layout.findViewById(R.id.aclanysourceext);
        aclAnyDestExt = (TextView) layout.findViewById(R.id.aclanydestext);
        aclSourceIPExt = (TextView) layout.findViewById(R.id.aclsourceipext);
        aclMatchingCondExt = (TextView) layout.findViewById(R.id.aclmatchingcondext);
        aclDestIPExt = (TextView) layout.findViewById(R.id.acldestinationipext);
        aclNumberExtET = (EditText) layout.findViewById(R.id.aclnumberextET);
        aclNumberExtET.addTextChangedListener(new ExtendedACLNumberValidation());
        aclOtherProtocolET = (EditText) layout.findViewById(R.id.aclotherprotocolET);
        aclSourceIPExtET1 = (EditText) layout.findViewById(R.id.aclsourceipextET1);
        aclSourceIPExtET1.addTextChangedListener(new IPv4Validation());
        aclSourceIPExtET2 = (EditText) layout.findViewById(R.id.aclsourceipextET2);
        aclSourceIPExtET2.addTextChangedListener(new IPv4Validation());
        aclSourceIPExtET3 = (EditText) layout.findViewById(R.id.aclsourceipextET3);
        aclSourceIPExtET3.addTextChangedListener(new IPv4Validation());
        aclSourceIPExtET4 = (EditText) layout.findViewById(R.id.aclsourceipextET4);
        aclSourceIPExtET4.addTextChangedListener(new IPv4Validation());
        aclDestinationIPExtET1 = (EditText) layout.findViewById(R.id.acldestinationipextET1);
        aclDestinationIPExtET1.addTextChangedListener(new IPv4Validation());
        aclDestinationIPExtET2 = (EditText) layout.findViewById(R.id.acldestinationipextET2);
        aclDestinationIPExtET2.addTextChangedListener(new IPv4Validation());
        aclDestinationIPExtET3 = (EditText) layout.findViewById(R.id.acldestinationipextET3);
        aclDestinationIPExtET3.addTextChangedListener(new IPv4Validation());
        aclDestinationIPExtET4 = (EditText) layout.findViewById(R.id.acldestinationipextET4);
        aclDestinationIPExtET4.addTextChangedListener(new IPv4Validation());
        aclWildcardExtET1 = (EditText) layout.findViewById(R.id.aclwildcardextET1);
        aclWildcardExtET1.addTextChangedListener(new IPv4Validation());
        aclWildcardExtET2 = (EditText) layout.findViewById(R.id.aclwildcardextET2);
        aclWildcardExtET2.addTextChangedListener(new IPv4Validation());
        aclWildcardExtET3 = (EditText) layout.findViewById(R.id.aclwildcardextET3);
        aclWildcardExtET3.addTextChangedListener(new IPv4Validation());
        aclWildcardExtET4 = (EditText) layout.findViewById(R.id.aclwildcardextET4);
        aclWildcardExtET4.addTextChangedListener(new IPv4Validation());
        aclWildcardExt2ET1 = (EditText) layout.findViewById(R.id.aclwildcardext2ET1);
        aclWildcardExt2ET1.addTextChangedListener(new IPv4Validation());
        aclWildcardExt2ET2 = (EditText) layout.findViewById(R.id.aclwildcardext2ET2);
        aclWildcardExt2ET2.addTextChangedListener(new IPv4Validation());
        aclWildcardExt2ET3 = (EditText) layout.findViewById(R.id.aclwildcardext2ET3);
        aclWildcardExt2ET3.addTextChangedListener(new IPv4Validation());
        aclWildcardExt2ET4 = (EditText) layout.findViewById(R.id.aclwildcardext2ET4);
        aclWildcardExt2ET4.addTextChangedListener(new IPv4Validation());

        ipv6Validation = new IPv6Validation();

        sourceIPv6ExtET1 = (EditText) layout.findViewById(R.id.aclsourceipv6extET1);
        sourceIPv6ExtET1.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET2 = (EditText) layout.findViewById(R.id.aclsourceipv6extET2);
        sourceIPv6ExtET2.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET3 = (EditText) layout.findViewById(R.id.aclsourceipv6extET3);
        sourceIPv6ExtET3.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET4 = (EditText) layout.findViewById(R.id.aclsourceipv6extET4);
        sourceIPv6ExtET4.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET5 = (EditText) layout.findViewById(R.id.aclsourceipv6extET5);
        sourceIPv6ExtET5.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET6 = (EditText) layout.findViewById(R.id.aclsourceipv6extET6);
        sourceIPv6ExtET6.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET7 = (EditText) layout.findViewById(R.id.aclsourceipv6extET7);
        sourceIPv6ExtET7.addTextChangedListener(ipv6Validation);
        sourceIPv6ExtET8 = (EditText) layout.findViewById(R.id.aclsourceipv6extET8);
        sourceIPv6ExtET8.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET1 = (EditText) layout.findViewById(R.id.acldestinationipv6extET1);
        destinationIPv6ExtET1.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET2 = (EditText) layout.findViewById(R.id.acldestinationipv6extET2);
        destinationIPv6ExtET2.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET3 = (EditText) layout.findViewById(R.id.acldestinationipv6extET3);
        destinationIPv6ExtET3.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET4 = (EditText) layout.findViewById(R.id.acldestinationipv6extET4);
        destinationIPv6ExtET4.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET5 = (EditText) layout.findViewById(R.id.acldestinationipv6extET5);
        destinationIPv6ExtET5.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET6 = (EditText) layout.findViewById(R.id.acldestinationipv6extET6);
        destinationIPv6ExtET6.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET7 = (EditText) layout.findViewById(R.id.acldestinationipv6extET7);
        destinationIPv6ExtET7.addTextChangedListener(ipv6Validation);
        destinationIPv6ExtET8 = (EditText) layout.findViewById(R.id.acldestinationipv6extET8);
        destinationIPv6ExtET8.addTextChangedListener(ipv6Validation);
        aclFromExtET = (EditText) layout.findViewById(R.id.aclfromextET);
        aclFromExtET.addTextChangedListener(new PortValidation());
        aclToExtET = (EditText) layout.findViewById(R.id.acltoextET);
        aclToExtET.addTextChangedListener(new PortValidation());
        aclTcpAppExtET = (EditText) layout.findViewById(R.id.acltcpappextET);
        aclNameET = (EditText) layout.findViewById(R.id.aclnameextET);
        sourceMaskIPv6ExtET = (EditText) layout.findViewById(R.id.aclsourcemaskipv6extET);
        sourceMaskIPv6ExtET.addTextChangedListener(new IPv6MaskValidation());
        destinationMaskIPv6ExtET = (EditText) layout.findViewById(R.id.acldestinationmaskipv6extET);
        destinationMaskIPv6ExtET.addTextChangedListener(new IPv6MaskValidation());
        aclDenyPermitExt = (RadioGroup) layout.findViewById(R.id.acldenypermitext);
        aclIPv4IPv6Ext = (RadioGroup) layout.findViewById(R.id.aclip4ip6ext);
        ipv4Ext = (RadioButton) layout.findViewById(R.id.aclipv4ext);
        ipv4Ext.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onIPSelectionClick(v);
			}
        });

    	ipv6Ext = (RadioButton) layout.findViewById(R.id.aclipv6ext);
    	ipv6Ext.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onIPSelectionClick(v);
			}
        });

        aclLogExtChB = (CheckBox) layout.findViewById(R.id.acllogextChB);
        aclRangeExtChB = (CheckBox) layout.findViewById(R.id.aclrangeextChB);
        aclRangeExtChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onRangeClick(v);
			}
        });

        aclAnySourceExtChB = (CheckBox) layout.findViewById(R.id.aclanysourceextChB);
        aclAnySourceExtChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAnyClick(v);
			}
        });

        aclAnyDestExtChB = (CheckBox) layout.findViewById(R.id.aclanydestextChB);
        aclAnyDestExtChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAnyClick(v);
			}
        });

        aclAnySourceIPv6ExtChB = (CheckBox) layout.findViewById(R.id.aclanysourceipv6extChB);
        aclAnySourceIPv6ExtChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAnyClick(v);
			}
        });

        aclAnyDestIPv6ExtChB = (CheckBox) layout.findViewById(R.id.aclanydestipv6extChB);
        aclAnyDestIPv6ExtChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAnyClick(v);
			}
        });

        otherProtocolChB = (CheckBox) layout.findViewById(R.id.aclotherprotocolChB);
        otherProtocolChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onOtherProtocolClick(v);
			}
        });

        aclNamedTypeChB = (CheckBox) layout.findViewById(R.id.aclnamedextChB);
        aclNamedTypeChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onNamedTypeClick(v);
			}
        });

        sourceIPv4ExtRL = (RelativeLayout) layout.findViewById(R.id.aclsourceipextRL);
    	destinationIPv4ExtRL = (RelativeLayout) layout.findViewById(R.id.acldestinationipextRL);
    	sourceIPv6ExtRL = (RelativeLayout) layout.findViewById(R.id.aclsourceipv6extRL);
    	destinationIPv6ExtRL = (RelativeLayout) layout.findViewById(R.id.acldestinationipv6extRL);
    	sourceWildcardExtRL = (RelativeLayout) layout.findViewById(R.id.aclsourceipextWRL);
    	destinationWildcardExtRL = (RelativeLayout) layout.findViewById(R.id.acldestinationipextWRL);
    	sourceMaskExtRL = (RelativeLayout) layout.findViewById(R.id.aclsourcemaskextRL);
    	destinationMaskExtRL = (RelativeLayout) layout.findViewById(R.id.acldestinationmaskextRL);
    	destinationIPHeader = (RelativeLayout) layout.findViewById(R.id.acldestinationipextRLH);
    	matchingConditionHeader = (RelativeLayout) layout.findViewById(R.id.aclmatchingcondextRLH);

    	aclMatchingCondExtP = (LayoutParams) matchingConditionHeader.getLayoutParams();
    	destinationIPExtP = (LayoutParams) destinationIPHeader.getLayoutParams();

        sourceIPv4ExtControls = new View[]{aclPointExt1, aclPointExt2, aclPointExt3,
    			aclSourceIPExtET1, aclSourceIPExtET2, aclSourceIPExtET3, aclSourceIPExtET4,
    			aclPointExt10, aclPointExt11, aclPointExt12, aclWildcardExt2ET1, aclWildcardExt2ET2,
    			aclWildcardExt2ET3, aclWildcardExt2ET4};

        destIPv4ExtControls = new View[]{aclPointExt4, aclPointExt5, aclPointExt6,
    			aclPointExt7, aclPointExt8, aclPointExt9, aclDestinationIPExtET1,
    			aclDestinationIPExtET2, aclDestinationIPExtET3, aclDestinationIPExtET4, aclWildcardExtET1,
    			aclWildcardExtET2, aclWildcardExtET3, aclWildcardExtET4};

        sourceIPv6ExtControls = new View[]{doublePointExt1, doublePointExt2, doublePointExt3, doublePointExt4,
        		doublePointExt5, doublePointExt6, doublePointExt7, sourceIPv6ExtET1, sourceIPv6ExtET2,
        		sourceIPv6ExtET3, sourceIPv6ExtET4, sourceIPv6ExtET5, sourceIPv6ExtET6, sourceIPv6ExtET7,
        		sourceIPv6ExtET8};

        destIPv6ExtControls = new View[]{doublePointExt8, doublePointExt9, doublePointExt10, doublePointExt11,
        		doublePointExt12, doublePointExt13, doublePointExt14, destinationIPv6ExtET1, destinationIPv6ExtET2,
    			destinationIPv6ExtET3, destinationIPv6ExtET4, destinationIPv6ExtET5, destinationIPv6ExtET6,
    			destinationIPv6ExtET7, destinationIPv6ExtET8};

        sourceIPv4ExtControlsET = new EditText[]{aclSourceIPExtET1, aclSourceIPExtET2, aclSourceIPExtET3,
        		aclSourceIPExtET4};
    	destinationIPv4ExtControlsET = new EditText[]{aclDestinationIPExtET1, aclDestinationIPExtET2,
    			aclDestinationIPExtET3, aclDestinationIPExtET4};
    	sourceWildcardExtControlsET = new EditText[]{aclWildcardExt2ET1, aclWildcardExt2ET2,
    			aclWildcardExt2ET3, aclWildcardExt2ET4};
    	destinationWildcardExtControlsET = new EditText[]{aclWildcardExtET1, aclWildcardExtET2,
    			aclWildcardExtET3, aclWildcardExtET4};
        sourceIPv6ExtControlsET = new EditText[]{sourceIPv6ExtET1, sourceIPv6ExtET2, sourceIPv6ExtET3,
        		sourceIPv6ExtET4, sourceIPv6ExtET5, sourceIPv6ExtET6, sourceIPv6ExtET7, sourceIPv6ExtET8};
    	destinationIPv6ExtControlsET = new EditText[]{destinationIPv6ExtET1, destinationIPv6ExtET2,
    			destinationIPv6ExtET3, destinationIPv6ExtET4, destinationIPv6ExtET5, destinationIPv6ExtET6,
    			destinationIPv6ExtET7, destinationIPv6ExtET8};

    	aclNumberErase = (ImageButton) layout.findViewById(R.id.aclnumberexterase);
    	aclNumberErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	aclProtocolErase = (ImageButton) layout.findViewById(R.id.aclprotocolexterase);
    	aclProtocolErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	aclSourceIPErase = (ImageButton) layout.findViewById(R.id.aclsourceipexterase);
    	aclSourceIPErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	aclDestinationIPErase = (ImageButton) layout.findViewById(R.id.acldestinationipexterase);
    	aclDestinationIPErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	aclMatchingConditionErase = (ImageButton) layout.findViewById(R.id.aclmatchingcondexterase);
    	aclMatchingConditionErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	aclTcpAppErase = (ImageButton) layout.findViewById(R.id.acltcpappexterase);
    	aclTcpAppErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

    	generate = (Button) layout.findViewById(R.id.aclgenerateext);
    	generate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onExtendedGenerateClick(v);
			}
        });

    	aclNameErase = (ImageButton) layout.findViewById(R.id.aclnameexterase);
        aclNameErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

        setEnabledAndFocusable(aclNameET, false);
		setEnabledAndFocusable(aclFromExtET, false);
		setEnabledAndFocusable(aclToExtET, false);
		setEnabledAndFocusable(ipv4Ext, false);
		setEnabledAndFocusable(ipv6Ext, false);
		setEnabledAndFocusable(aclOtherProtocolET, false);

		SharedPreferences preferences = this.getActivity().getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("type", "acl");
		editor.commit();
    }
}
