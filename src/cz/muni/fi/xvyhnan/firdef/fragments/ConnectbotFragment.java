/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.fragments;

import java.util.List;

import org.connectbot.ColorsActivity;
import org.connectbot.ConsoleActivity;
import org.connectbot.HelpActivity;
import org.connectbot.HostEditorActivity;
import org.connectbot.PortForwardListActivity;
import org.connectbot.PubkeyListActivity;
import org.connectbot.SettingsActivity;
import org.connectbot.WizardActivity;
import org.connectbot.bean.HostBean;
import org.connectbot.service.TerminalBridge;
import org.connectbot.service.TerminalManager;
import org.connectbot.transport.TransportFactory;
import org.connectbot.util.HostDatabase;
import org.connectbot.util.PreferenceConstants;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;

import cz.muni.fi.xvyhnan.firdef.R;

/**
 * Fragment showing ConnectBot.
 * @author Jan Vyhn�nek, Kenny Root, Jeffrey Sharkey
 */
public class ConnectbotFragment extends SherlockListFragment {
	public static final String EXTRA_TITLE = "CONNECTBOT";
	private View layout;
	public final static int REQUEST_EDIT = 1;
	public final static int REQUEST_EULA = 2;
	protected TerminalManager bound = null;
	protected HostDatabase hostdb;
	private List<HostBean> hosts;
	protected LayoutInflater inflater = null;
	protected boolean sortedByColor = false;
	private MenuItem sortcolor, sortlast;
	private Spinner transportSpinner;
	private TextView quickconnect;
	private SharedPreferences prefs = null;
	protected boolean makingShortcut = false;
	private ActionMode mode;

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

    	View v = new View(getActivity());
        v = inflater.inflate(R.layout.connectbotfragment, container, false);
        layout = v;
        return v;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

    	initialization();
    	setHasOptionsMenu(true);
    }

    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }

	protected Handler updateHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			ConnectbotFragment.this.updateList();
		}
	};

	private ServiceConnection connection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			bound = ((TerminalManager.TerminalBinder) service).getService();

			// update our listview binder to find the service
			ConnectbotFragment.this.updateList();
		}

		public void onServiceDisconnected(ComponentName className) {
			bound = null;
			ConnectbotFragment.this.updateList();
		}
	};

	@Override
	public void onStart() {
		super.onStart();

		// start the terminal manager service
		getActivity().bindService(new Intent(getActivity(), TerminalManager.class), connection, Context.BIND_AUTO_CREATE);

		if(this.hostdb == null)
			this.hostdb = new HostDatabase(getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();
		getActivity().unbindService(connection);

		if(this.hostdb != null) {
			this.hostdb.close();
			this.hostdb = null;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_EULA) {
			if(resultCode == Activity.RESULT_OK) {
				// yay they agreed, so store that info
				Editor edit = prefs.edit();
				edit.putBoolean(PreferenceConstants.EULA, true);
				edit.commit();
			} else {
				// user didnt agree, so close
				getActivity().finish();
			}
		} else if (requestCode == REQUEST_EDIT) {
			this.updateList();
		}
	}

    public void initialization() {
		// check for eula agreement
		this.prefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());

		boolean agreed = prefs.getBoolean(PreferenceConstants.EULA, false);
		if (!agreed) {
			this.startActivityForResult(new Intent(getActivity(),
					WizardActivity.class), REQUEST_EULA);
		}

		this.makingShortcut = Intent.ACTION_CREATE_SHORTCUT
				.equals(getActivity().getIntent().getAction())
				|| Intent.ACTION_PICK.equals(getActivity().getIntent()
						.getAction());

		// connect with hosts database and populate list
		this.hostdb = new HostDatabase(getActivity());
		ListView list = this.getListView();

		this.sortedByColor = prefs.getBoolean(
				PreferenceConstants.SORT_BY_COLOR, false);

		// this.list.setSelector(R.drawable.highlight_disabled_pressed);

		list.setOnItemClickListener(new OnItemClickListener() {

			public synchronized void onItemClick(AdapterView<?> parent,
					View view, int position, long id) {

				// launch off to console details
				HostBean host = (HostBean) parent.getAdapter()
						.getItem(position);
				Uri uri = host.getUri();

				Intent contents = new Intent(Intent.ACTION_VIEW, uri);
				contents.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				if (makingShortcut) {
					// create shortcut if requested
					ShortcutIconResource icon = Intent.ShortcutIconResource
							.fromContext(getActivity(), R.drawable.icon);

					Intent intent = new Intent();
					intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, contents);
					intent.putExtra(Intent.EXTRA_SHORTCUT_NAME,
							host.getNickname());
					intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);

					getActivity().setResult(getActivity().RESULT_OK, intent);
					getActivity().finish();

				} else {
					// otherwise just launch activity to show this host
					getActivity().startActivity(contents);
				}
			}
		});

		this.registerForContextMenu(list);

		quickconnect = (TextView) layout.findViewById(R.id.front_quickconnect);
		quickconnect.setVisibility(makingShortcut ? View.GONE : View.VISIBLE);
		quickconnect.setOnKeyListener(new OnKeyListener() {

			public boolean onKey(View v, int keyCode, KeyEvent event) {

				if (event.getAction() == KeyEvent.ACTION_UP)
					return false;
				if (keyCode != KeyEvent.KEYCODE_ENTER)
					return false;

				return startConsoleActivity();
			}
		});

		transportSpinner = (Spinner) layout
				.findViewById(R.id.transport_selection);
		ArrayAdapter<String> transportSelection = new ArrayAdapter<String>(
				getActivity(), android.R.layout.simple_spinner_item,
				TransportFactory.getTransportNames());
		transportSelection
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		transportSpinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> arg0, View view,
							int position, long id) {
						String formatHint = TransportFactory.getFormatHint(
								(String) transportSpinner.getSelectedItem(),
								getActivity());

						quickconnect.setHint(formatHint);
						quickconnect.setError(null);
						quickconnect.requestFocus();
					}

					public void onNothingSelected(AdapterView<?> arg0) {
					}
				});
		transportSpinner.setAdapter(transportSelection);

		this.inflater = LayoutInflater.from(getActivity());
    }

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    super.onCreateOptionsMenu(menu, inflater);

		// don't offer menus when creating shortcut
	    MenuItem more = menu.add(R.string.more);
	    more.setIcon(android.R.drawable.ic_menu_more);
	    more.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	    more.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				mode = getSherlockActivity().startActionMode(new AnActionModeOfEpicProportions());
				return true;
			}
	    });
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

		// create menu to handle hosts

		// create menu to handle deleting and sharing lists
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		final HostBean host = (HostBean) this.getListView().getItemAtPosition(info.position);

		menu.setHeaderTitle(host.getNickname());

		// edit, disconnect, delete
		android.view.MenuItem connect = menu.add(R.string.list_host_disconnect);
		final TerminalBridge bridge = bound.getConnectedBridge(host);
		connect.setEnabled((bridge != null));
		connect.setOnMenuItemClickListener(new android.view.MenuItem.OnMenuItemClickListener() {
			public boolean onMenuItemClick(android.view.MenuItem item) {
				bridge.dispatchDisconnect(true);
				updateHandler.sendEmptyMessage(-1);
				return true;
			}
		});

		final SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		android.view.MenuItem edit = menu.add(R.string.list_host_edit);
		edit.setOnMenuItemClickListener(new android.view.MenuItem.OnMenuItemClickListener() {
			public boolean onMenuItemClick(android.view.MenuItem item) {
				Intent intent = new Intent(getActivity(), HostEditorActivity.class);
				intent.putExtra(Intent.EXTRA_TITLE, host.getId());
				intent.putExtra("apptheme", colorPreferences.getString("apptheme", "Black"));
				getActivity().startActivityForResult(intent, REQUEST_EDIT);
				return true;
			}
		});

		android.view.MenuItem portForwards = menu.add(R.string.list_host_portforwards);
		portForwards.setOnMenuItemClickListener(new android.view.MenuItem.OnMenuItemClickListener() {
			public boolean onMenuItemClick(android.view.MenuItem item) {
				Intent intent = new Intent(getActivity(), PortForwardListActivity.class);
				intent.putExtra(Intent.EXTRA_TITLE, host.getId());
				getActivity().startActivityForResult(intent, REQUEST_EDIT);
				return true;
			}
		});
		if (!TransportFactory.canForwardPorts(host.getProtocol()))
			portForwards.setEnabled(false);

		android.view.MenuItem delete = menu.add(R.string.list_host_delete);
		delete.setOnMenuItemClickListener(new android.view.MenuItem.OnMenuItemClickListener() {
			public boolean onMenuItemClick(android.view.MenuItem item) {
				// prompt user to make sure they really want this
				new AlertDialog.Builder(getActivity())
					.setMessage(getString(R.string.delete_message, host.getNickname()))
					.setPositiveButton(R.string.delete_pos, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						// make sure we disconnect
							if(bridge != null)
								bridge.dispatchDisconnect(true);

							hostdb.deleteHost(host);
							updateHandler.sendEmptyMessage(-1);
						}
						})
					.setNegativeButton(R.string.delete_neg, null).create().show();

				return true;
			}
		});
	}

	private boolean startConsoleActivity() {
		Uri uri = TransportFactory.getUri((String) transportSpinner
				.getSelectedItem(), quickconnect.getText().toString());

		if (uri == null) {
			quickconnect.setError(getString(R.string.list_format_error,
					TransportFactory.getFormatHint(
							(String) transportSpinner.getSelectedItem(),
							getActivity())));
			return false;
		}

		HostBean host = TransportFactory.findHost(hostdb, uri);
		if (host == null) {
			host = TransportFactory.getTransport(uri.getScheme()).createHost(uri);
			host.setColor(HostDatabase.COLOR_GRAY);
			host.setPubkeyId(HostDatabase.PUBKEYID_ANY);
			hostdb.saveHost(host);
		}

		Intent intent = new Intent(getActivity(), ConsoleActivity.class);
		intent.setData(uri);
		startActivity(intent);

		return true;
	}

	protected void updateList() {
		if (prefs.getBoolean(PreferenceConstants.SORT_BY_COLOR, false) != sortedByColor) {
			Editor edit = prefs.edit();
			edit.putBoolean(PreferenceConstants.SORT_BY_COLOR, sortedByColor);
			edit.commit();
		}

		if (hostdb == null)
			hostdb = new HostDatabase(getActivity());

		hosts = hostdb.getHosts(sortedByColor);

		// Don't lose hosts that are connected via shortcuts but not in the database.
		if (bound != null) {
			for (TerminalBridge bridge : bound.bridges) {
				if (!hosts.contains(bridge.host))
					hosts.add(0, bridge.host);
			}
		}

		HostAdapter adapter = new HostAdapter(getActivity(), hosts, bound);

		this.setListAdapter(adapter);
	}

	class HostAdapter extends ArrayAdapter<HostBean> {
		private List<HostBean> hosts;
		private final TerminalManager manager;
		private final ColorStateList red, green, blue;

		public final static int STATE_UNKNOWN = 1, STATE_CONNECTED = 2, STATE_DISCONNECTED = 3;

		class ViewHolder {
			public TextView nickname;
			public TextView caption;
			public ImageView icon;
		}

		public HostAdapter(Context context, List<HostBean> hosts, TerminalManager manager) {
			super(context, R.layout.item_host, hosts);

			this.hosts = hosts;
			this.manager = manager;

			red = context.getResources().getColorStateList(R.color.red);
			green = context.getResources().getColorStateList(R.color.green);
			blue = context.getResources().getColorStateList(R.color.blue);
		}

		/**
		 * Check if we're connected to a terminal with the given host.
		 */
		private int getConnectedState(HostBean host) {
			// always disconnected if we dont have backend service
			if (this.manager == null)
				return STATE_UNKNOWN;

			if (manager.getConnectedBridge(host) != null)
				return STATE_CONNECTED;

			if (manager.disconnected.contains(host))
				return STATE_DISCONNECTED;

			return STATE_UNKNOWN;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_host, null, false);

				holder = new ViewHolder();

				holder.nickname = (TextView)convertView.findViewById(android.R.id.text1);
				holder.caption = (TextView)convertView.findViewById(android.R.id.text2);
				holder.icon = (ImageView)convertView.findViewById(android.R.id.icon);

				convertView.setTag(holder);
			} else
				holder = (ViewHolder) convertView.getTag();

			HostBean host = hosts.get(position);
			if (host == null) {
				// Well, something bad happened. We can't continue.
				Log.e("HostAdapter", "Host bean is null!");

				holder.nickname.setText("Error during lookup");
				holder.caption.setText("see 'adb logcat' for more");
				return convertView;
			}

			holder.nickname.setText(host.getNickname());

			switch (this.getConnectedState(host)) {
			case STATE_UNKNOWN:
				holder.icon.setImageState(new int[] { }, true);
				break;
			case STATE_CONNECTED:
				holder.icon.setImageState(new int[] { android.R.attr.state_checked }, true);
				break;
			case STATE_DISCONNECTED:
				holder.icon.setImageState(new int[] { android.R.attr.state_expanded }, true);
				break;
			}

			ColorStateList chosen = null;
			if (HostDatabase.COLOR_RED.equals(host.getColor()))
				chosen = this.red;
			else if (HostDatabase.COLOR_GREEN.equals(host.getColor()))
				chosen = this.green;
			else if (HostDatabase.COLOR_BLUE.equals(host.getColor()))
				chosen = this.blue;

			Context context = convertView.getContext();

			if (chosen != null) {
				// set color normally if not selected
				holder.nickname.setTextColor(chosen);
				holder.caption.setTextColor(chosen);
			} else {
				// selected, so revert back to default black text
				holder.nickname.setTextAppearance(context, android.R.attr.textAppearanceLarge);
				holder.caption.setTextAppearance(context, android.R.attr.textAppearanceSmall);
			}

			long now = System.currentTimeMillis() / 1000;

			String nice = context.getString(R.string.bind_never);
			if (host.getLastConnect() > 0) {
				int minutes = (int)((now - host.getLastConnect()) / 60);
				if (minutes >= 60) {
					int hours = (minutes / 60);
					if (hours >= 24) {
						int days = (hours / 24);
						nice = context.getString(R.string.bind_days, days);
					} else
						nice = context.getString(R.string.bind_hours, hours);
				} else
					nice = context.getString(R.string.bind_minutes, minutes);
			}

			holder.caption.setText(nice);

			return convertView;
		}
	}

	/**
	 * Handles action bar overflow menu.
	 */
	private final class AnActionModeOfEpicProportions implements ActionMode.Callback {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        	if(makingShortcut) return false;
        	MenuInflater inflater = getSherlockActivity().getSupportMenuInflater();
    		inflater.inflate(R.menu.connectbotmainmenu, menu);
    		return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			mode.finish();

			switch (item.getItemId()) {
			case R.id.sortcolormenu:
				if(sortcolor == null) sortcolor = item;
				break;
			case R.id.sortlast:
				if(sortlast == null) sortlast = item;
				break;
			}

			// don't offer menus when creating shortcut
			if (makingShortcut) return false;

			if(sortcolor != null && item.getItemId() == R.id.sortcolormenu) {
				item.setVisible(!sortedByColor);
			}
			if(sortlast != null && item.getItemId() == R.id.sortlast) {
				item.setVisible(sortedByColor);
			}
			return prepareOnActionModeClickMenu(item);
        }

        public void onDestroyActionMode(ActionMode mode) {
        }
	}

	/**
	 * Reacts when menu item is pressed.
	 * @param item Selected menu item.
	 */
	public boolean prepareOnActionModeClickMenu(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.sortcolormenu:
			sortedByColor = true;
			updateList();
			return true;
		case R.id.sortlast:
			sortedByColor = false;
			updateList();
			return true;
		case R.id.keys:
			startActivity(new Intent(getActivity(), PubkeyListActivity.class));
			return true;
		case R.id.colors:
			startActivity(new Intent(getActivity(), ColorsActivity.class));
			return true;
		case R.id.cbsettings:
			startActivity(new Intent(getActivity(), SettingsActivity.class));
			return true;
		case R.id.cbhelp:
			startActivity(new Intent(getActivity(), HelpActivity.class));
			return true;
		}
		return true;
	}
}
