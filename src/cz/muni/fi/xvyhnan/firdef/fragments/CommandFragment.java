/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import cz.muni.fi.xvyhnan.components.CustomAlertDialogBuilder;
import cz.muni.fi.xvyhnan.firdef.R;
import cz.muni.fi.xvyhnan.firdef.database.DBManager;

/**
 * Fragment showing generated command.
 * @author Jan Vyhn�nek
 */
public class CommandFragment extends Fragment {
	public static final String EXTRA_TITLE = "COMMAND";
	private View layout;
	private TextView generatedCommand;
	private DBManager dbManager;
	private String selectedDBRule = null;
	private Button selectedDBRuleBTN = null, addToDB, showDB, editCmd, restoreCmd;
	private String theme = "Black";

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

    	View v = new View(getActivity());
        v = inflater.inflate(R.layout.commandfragment, container, false);
        layout = v;
        return v;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

    	initialization();
    }

    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }

    public void initialization() {
        generatedCommand = (TextView) layout.findViewById(R.id.generatedcommand);

        addToDB = (Button) layout.findViewById(R.id.addtodb);
        addToDB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAddToDBClick(v);
			}
        });

        showDB = (Button) layout.findViewById(R.id.showdb);
        showDB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onShowDBClick(v);
			}
        });

        editCmd = (Button) layout.findViewById(R.id.editcmd);
        editCmd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEditCmdClick(v);
			}
        });

        restoreCmd = (Button) layout.findViewById(R.id.restorecmd);
        restoreCmd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onRestoreCmdClick(v);
			}
        });

        SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();

        if(getActivity().getIntent().hasExtra("precommand")) {
        	generatedCommand.setText(getActivity().getIntent().getExtras().getCharSequence("precommand") +"\n\n"
        			+ getActivity().getIntent().getExtras().getCharSequence("command"));
        	editor.putString("precommand", getActivity().getIntent().getExtras().getCharSequence("precommand").toString());
        } else {
        	generatedCommand.setText(getActivity().getIntent().getExtras().getCharSequence("command"));
        }
        editor.putString("command", getActivity().getIntent().getExtras().getCharSequence("command").toString());
        editor.commit();

        //inicializace datab�ze
        dbManager = new DBManager(getActivity());
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();

    	if(getActivity().isFinishing()) {
    		SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
    		SharedPreferences.Editor editor = preferences.edit();
    		editor.putString("precommand", "");
    		editor.commit();

    		//zav�r�n� spojen� s datab�zi
    		try {
    			dbManager.closeDB();
    		} catch(Exception e) {
    			Toast.makeText(getActivity(), R.string.closedbinfo, Toast.LENGTH_LONG).show();
    		}
    	}
    }

    /**
    * Restores the generated rule.
    * @param v Pressed button.
    */
   public void onRestoreCmdClick(View v) {
	   SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
	   SharedPreferences.Editor editor = preferences.edit();

       if(getActivity().getIntent().hasExtra("precommand")) {
       		generatedCommand.setText(getActivity().getIntent().getExtras().getCharSequence("precommand") +"\n\n"
       			+ getActivity().getIntent().getExtras().getCharSequence("command"));
       		editor.putString("precommand", getActivity().getIntent().getExtras().getCharSequence("precommand").toString());
       } else {
    	   	editor.putString("precommand", "");
       		generatedCommand.setText(getActivity().getIntent().getExtras().getCharSequence("command"));
       }
       editor.putString("command", getActivity().getIntent().getExtras().getCharSequence("command").toString());
       editor.commit();
   }

    /**
     * Edits the generated rule.
     * @param v Pressed button.
     */
    public void onEditCmdClick(View v) {
    	final EditText editedCmd = new EditText(getActivity());
    	final LinearLayout layout = new LinearLayout(getActivity());
    	editedCmd.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    	editedCmd.setText(generatedCommand.getText().toString());
    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    	params.setMargins(10, 0, 10, 0);
    	editedCmd.setLayoutParams(params);
    	layout.addView(editedCmd);

    	final SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
    	if(preferences.contains("precommand") && !preferences.getString("precommand", "").equals("")) {
    		new AlertDialog.Builder(getActivity())
    		.setMessage("Do you want to edit the generated pre-command or command?")
    		.setPositiveButton("Pre-command", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				showCreateDialog(true);
    			}
    		})
    		.setNegativeButton("Command", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				showCreateDialog(false);
    			}
    		}).show();
    	} else {
    		new AlertDialog.Builder(getActivity())
    		.setMessage("Edit the generated command, but be carefull, it will not be validated!")
    		.setView(layout)
    		.setPositiveButton("Save", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				if(editedCmd.getText().toString().replaceAll(" ", "").equals("")) {
    					Toast.makeText(getActivity(), R.string.toastcommandnoempty, Toast.LENGTH_LONG).show();
    				} else {
    					generatedCommand.setText(editedCmd.getText().toString());
    		 	   		SharedPreferences.Editor editor = preferences.edit();
    		 	   		editor.putString("command", generatedCommand.getText().toString());
    					editor.commit();
    				}
    			}
    		})
    		.setNegativeButton("Back", null).show();
    	}
    }

    /**
     * Adds generated command to database.
     * @param v Pressed button.
     */
    public void onAddToDBClick(View v) {
    	try {
    		SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);

    		String cmd = preferences.getString("command", "");
    		String type = preferences.getString("type", "").replaceAll(" ", "");

    		String insert = "";
    		if(preferences.contains("precommand") && !preferences.getString("precommand", "").replaceAll(" ", "").equals("")) {
    			String precmd = preferences.getString("precommand", "");
    			insert = "INSERT INTO Rules(Rule, PreRule, Type) VALUES('"+ cmd +"', '"+ precmd +"', '"+ type +"')";
    		} else {
    			insert = "INSERT INTO Rules(Rule, Type) VALUES('"+ cmd +"', '"+ type +"')";
    		}
    		dbManager.getDB().execSQL(insert);

    		Toast.makeText(getActivity(), R.string.addtodbinfo, Toast.LENGTH_LONG).show();
    	} catch(Exception e) {
    		Toast.makeText(getActivity(), R.string.addtodbneginfo, Toast.LENGTH_LONG).show();
    	}

    }

    /**
     * Shows stored commands in database.
     * @param v Pressed button.
     */
    public void onShowDBClick(View v) {
    	selectedDBRule = null;
    	SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
		String type = preferences.getString("type", "").replaceAll(" ", "");
		Cursor cursor;
		if(type.equals("acl")) {
			cursor = dbManager.getDB().query("Rules", new String[]{"id","Rule","PreRule"}, "Type ='"+ type +"'", null, null, null, "Inserted DESC");
		} else {
			cursor = dbManager.getDB().query("Rules", new String[]{"id","Rule"}, "Type ='"+ type +"'", null, null, null, "Inserted DESC");
		}

		if(cursor.getCount() > 0) {
			TableLayout tableLayout = new TableLayout(getActivity().getApplicationContext());
	        tableLayout.removeAllViews();
	        ScrollView scrollView = new ScrollView(getActivity().getApplicationContext());

			cursor.moveToFirst();
			while(!cursor.isAfterLast()) {
				List<String> columns = new ArrayList<String>();
				columns.add(cursor.getString(0));
				columns.add(cursor.getString(1));
				if(type.equals("acl")) {
					try {
						if(!cursor.getString(2).toLowerCase().equals("null")) {
							columns.set(1, cursor.getString(2) +"\n\n"+ cursor.getString(1));
						}
					} catch(Exception e) {
						//OK zaznam neexistuje
					}
				}

				final int backgroundColor;
		    	if(theme.equals("Light")) {
		    		backgroundColor = Color.WHITE;
		    	} else {
		    		backgroundColor = Color.BLACK;
		    	}

				TableRow tableRow = new TableRow(getActivity().getApplicationContext());
				tableRow.setGravity(17);
				tableRow.setBackgroundColor(backgroundColor);

				//pripravim si oddelovac radku
		        View divider = new View(getActivity().getApplicationContext());
		        divider.setBackgroundColor(Color.rgb(255, 172, 0));

				int counter = 0;
				for(String column : columns) {
					final Button columsView = new Button(getActivity().getApplicationContext());
					final TextView idView = new TextView(getActivity().getApplicationContext());

	                if(counter == 0) {
	                	idView.setText(column);
	                	idView.setVisibility(4);
	                	idView.setMaxWidth(0);
	                	idView.setMaxHeight(0);

	                	tableRow.addView(idView);
	                } else {
	                	columsView.setText(column);
		                columsView.setTypeface(null, Typeface.BOLD);
		                columsView.setTextColor(Color.rgb(255, 172, 0));
		                columsView.setTextSize(16);
		                columsView.setGravity(17);
		                columsView.setBackgroundColor(backgroundColor);

	                	columsView.setOnClickListener(new View.OnClickListener() {
	                		public void onClick(View v) {
	                			if(selectedDBRuleBTN != null) {
	                				selectedDBRuleBTN.setTextColor(Color.rgb(255, 172, 0));
	                				selectedDBRuleBTN.setBackgroundColor(backgroundColor);
	                				((TableRow)selectedDBRuleBTN.getParent()).setBackgroundColor(backgroundColor);
	                			}

	                			Button selectedRow = (Button) v;
	                			if(selectedDBRuleBTN == selectedRow) {
	                				selectedDBRuleBTN = null;
	                				selectedRow.setTextColor(Color.rgb(255, 172, 0));
	                				selectedDBRule = null;
	                				selectedRow.setBackgroundColor(backgroundColor);
	                				((TableRow)selectedRow.getParent()).setBackgroundColor(backgroundColor);
	                			} else {
	                				selectedDBRuleBTN = selectedRow;
	                				selectedRow.setTextColor(backgroundColor);
	                				selectedDBRule = selectedRow.getText().toString();
	                				selectedRow.setBackgroundColor(Color.rgb(255, 172, 0));
	                				((TableRow)selectedRow.getParent()).setBackgroundColor(Color.rgb(255, 172, 0));
	                			}
	                		}
	                	});

	                	tableRow.addView(columsView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	                }

	                counter++;
				}

				tableLayout.addView(divider, LayoutParams.FILL_PARENT, 2);
				tableLayout.addView(tableRow, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				cursor.moveToNext();
				if(cursor.isAfterLast()) {
					View lastDivider = new View(getActivity().getApplicationContext());
					lastDivider.setBackgroundColor(Color.rgb(255, 172, 0));
					tableLayout.addView(lastDivider, LayoutParams.FILL_PARENT, 2);
				}
			}
			scrollView.addView(tableLayout);

			CustomAlertDialogBuilder dialog = new CustomAlertDialogBuilder(getActivity());
			dialog.setIcon(android.R.drawable.ic_menu_save);
			dialog.setTitle("Stored rules");
			dialog.setView(scrollView);
			dialog.setPositiveButton("Select", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(selectedDBRule == null) {
						Toast.makeText(getActivity(), R.string.norule, Toast.LENGTH_SHORT).show();
					} else {
						SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
    		 	   		SharedPreferences.Editor editor = preferences.edit();

						String command = "";
						if(selectedDBRule.contains("\n\n")) {
							String preCommand = "";
							String[] wholeRule = selectedDBRule.split("\n\n");
							try {
								preCommand = wholeRule[0];
								command = wholeRule[1];
								editor.putString("precommand", preCommand);
							} catch(Exception e) {
								Toast.makeText(getActivity(), R.string.toastaclspliterr, Toast.LENGTH_LONG).show();
							}
						} else {
							command = selectedDBRule;
							editor.putString("precommand", "");
						}
						generatedCommand.setText(selectedDBRule);

    		 	   		editor.putString("command", command);
    		 	   		editor.commit();
    		 	   		selectedDBRule = null;

    		 	   		dialog.dismiss();
					}
				}
			});
			dialog.setNeutralButton("Delete", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(selectedDBRule == null) {
						Toast.makeText(getActivity(), R.string.norule, Toast.LENGTH_SHORT).show();
					} else {
						String deletedRowID = ((TextView)((ViewGroup)selectedDBRuleBTN.getParent()).getChildAt(0)).getText().toString();
						showConfirmDialog(deletedRowID);
						dialog.dismiss();
					}
					selectedDBRule = null;
				}
			});
			dialog.setNegativeButton("Delete all", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					showConfirmDialog("-1");
					dialog.dismiss();
				}
			});
			dialog.setCanceledOnTouchOutside(true);
			dialog.show();
		} else {
			Toast.makeText(getActivity(), R.string.emptydb, Toast.LENGTH_LONG).show();
		}
		cursor.close();
    }

    /**
     * Shows the editing dialog.
     * @param preCommand Determines whether edit the pre-command or command.
     */
    private void showCreateDialog(boolean preCommand) {
    	final boolean preCommand2 = preCommand;

    	final EditText editedCmd = new EditText(getActivity());
    	final LinearLayout layout = new LinearLayout(getActivity());
    	editedCmd.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    	editedCmd.setText(generatedCommand.getText().toString());
    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    	params.setMargins(10, 0, 10, 0);
    	editedCmd.setLayoutParams(params);
    	layout.addView(editedCmd);

    	SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
    	if(preferences.contains("precommand") && !preferences.getString("precommand", "").equals("")) {
    		if(preCommand) {
    			editedCmd.setText(preferences.getString("precommand", ""));
    		} else {
    			editedCmd.setText(preferences.getString("command", ""));
    		}
    	}

    	new AlertDialog.Builder(getActivity())
		.setMessage("Edit the generated "+ (preCommand ? "pre-" : "") +"command, but be carefull, it will not be validated!")
		.setView(layout)
		.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
		 	   	SharedPreferences.Editor editor = preferences.edit();
				if(preferences.contains("precommand") && !preferences.getString("precommand", "").equals("")) {
		    		if(preCommand2) {
		    			if(editedCmd.getText().toString().replaceAll(" ", "").equals("")) {
		    				Toast.makeText(getActivity(), R.string.toastprecommandnoempty, Toast.LENGTH_LONG).show();
		    			} else {
		    				generatedCommand.setText(editedCmd.getText().toString() +"\n\n"
		    						+ preferences.getString("command", ""));
		    				editor.putString("precommand", editedCmd.getText().toString());
							editor.commit();
		    			}
		    		} else {
		    			if(editedCmd.getText().toString().replaceAll(" ", "").equals("")) {
		    				Toast.makeText(getActivity(), R.string.toastcommandnoempty, Toast.LENGTH_LONG).show();
		    			} else {
		    				generatedCommand.setText(preferences.getString("precommand", "")+ "\n\n" +
		    						editedCmd.getText().toString());
		    				editor.putString("command", editedCmd.getText().toString());
							editor.commit();
		    			}
		    		}
		    	}
			}
		})
		.setNegativeButton("Back", null).show();
    }

    /**
     * Shows the confirm dialog.
     * @param id ID of deleted row. If not positive, all rows of selected type are deleted.
     */
    private void showConfirmDialog(String id) {
    	if(Integer.valueOf(id) > 0) {
    		final String deletedRowID = id;
    		new AlertDialog.Builder(getActivity())
    			.setMessage(R.string.confirmdelete)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dbManager.getDB().execSQL("DELETE FROM Rules WHERE id ="+ deletedRowID);
						Toast.makeText(getActivity(), R.string.dbdelete, Toast.LENGTH_SHORT).show();
					}
				})
				.setNegativeButton("No", null).show();
    	} else {
    		new AlertDialog.Builder(getActivity())
			.setMessage(R.string.confirmdeleteall)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences preferences = getActivity().getSharedPreferences("preferences", 0);
		    		String type = preferences.getString("type", "").replaceAll(" ", "");
					dbManager.getDB().execSQL("DELETE FROM Rules WHERE Type ='"+ type +"'");
					Toast.makeText(getActivity(), R.string.dbdeleteall, Toast.LENGTH_SHORT).show();
				}
			})
			.setNegativeButton("No", null).show();
    	}
    }
}
