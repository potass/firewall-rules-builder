/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.fragments;

import java.util.HashSet;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import cz.muni.fi.xvyhnan.firdef.IConfiguration;
import cz.muni.fi.xvyhnan.firdef.InputDataValidation;
import cz.muni.fi.xvyhnan.firdef.R;
import cz.muni.fi.xvyhnan.firdef.ShowCommand;
import cz.muni.fi.xvyhnan.firdef.validation.IPv4Validation;
import cz.muni.fi.xvyhnan.firdef.validation.StandardACLNumberValidation;

/**
 * Fragment showing configuration of standard ACL.
 * @author Jan Vyhn�nek
 */
public class StandardACLFragment extends Fragment implements IConfiguration {
	public static final String EXTRA_TITLE = "STANDARD ACL";
	private final String ERROR = "!!!";
	private final int DEFAULT_TEXT_COLOR = Color.WHITE;
	private final int DEFAULT_TEXT_COLOR_WHITE = Color.BLACK;
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private final boolean DEFAULT_VALIDATION = true;
	private InputDataValidation validator;
	private View layout;
	private RadioGroup aclDenyPermit;
	private CheckBox aclLogChB, aclAnyChB, aclNamedTypeChB;
	private TextView aclPoint1, aclPoint2, aclPoint3, aclPoint4, aclPoint5, aclPoint6;
	private EditText aclNumberET, aclSourceIPET1, aclSourceIPET2, aclSourceIPET3, aclSourceIPET4,
			aclWildcardET1, aclWildcardET2, aclWildcardET3, aclWildcardET4, aclNameET;
	private View[] standardControls;
	private EditText[] sourceIPv4ControlsET, wildcardControlsET;
	private Button generate;
	private ImageButton aclNumberErase, aclSourceIPErase, aclNameErase;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

    	View v = new View(getActivity());
        v = inflater.inflate(R.layout.standardacl, container, false);
        layout = v;
        return v;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

        initialization();
    }

    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }

    @Override
    public void onResume() {
    	super.onResume();

    	for(EditText e : sourceIPv4ControlsET) {
			setError(e, false);
		}
		for(EditText e : wildcardControlsET) {
			setError(e, false);
		}

		setError(aclNumberET, false);
		setError(aclNameET, false);
    }

    /**
     * Validates inserted values, generates the filtering rule and shows it, considering the standard access lists.
     * @param v Pressed button.
     */
    public void onStandardGenerateClick(View v) {
    	boolean error = false;

    	Set<String> alert = new HashSet<String>();
    	String[] ipOctets = new String[]{aclSourceIPET1.getText().toString(), aclSourceIPET2.getText().toString(),
    			aclSourceIPET3.getText().toString(), aclSourceIPET4.getText().toString()};
    	String[] maskOctets = new String[]{aclWildcardET1.getText().toString(), aclWildcardET2.getText().toString(),
    			aclWildcardET3.getText().toString(), aclWildcardET4.getText().toString()};

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    	if(colorPreferences.getBoolean("validation", DEFAULT_VALIDATION)) {

			for (EditText e : sourceIPv4ControlsET) {
				setError(e, false);
			}
			for (EditText e : wildcardControlsET) {
				setError(e, false);
			}
			setError(aclNumberET, false);
			setError(aclNameET, false);

			if (aclNamedTypeChB.isChecked() && (aclNameET.getText().toString().replaceAll(" ", "").equals("")
					|| !isAcceptedValue(aclNameET.getText().toString()))) {
				setError(aclNameET, true);
				alert.add(getString(R.string.missingaclname) + "\n\n");
				error = true;
			}

			if (!aclNamedTypeChB.isChecked() && !validator.isAclNumber(aclNumberET)) {
				alert.add(getString(R.string.wrongaclnumber) + "\n\n");
				error = true;
			}

			if (!aclAnyChB.isChecked()
					&& !validator.isIpAddress(sourceIPv4ControlsET)) {
				alert.add(getString(R.string.wrongsourceip) + "\n\n");
				error = true;
			}

			if (!aclAnyChB.isChecked()
					&& !validator.isWildcard(wildcardControlsET)) {
				alert.add(getString(R.string.wrongsourceip) + "\n\n");
				error = true;
			}
		}

    	if(error) {
    		makeAlertDialog(alert);
    	} else {
    		String command = "";
    		String preCommand = "";
    		if(aclNamedTypeChB.isChecked()) {
    			command += denyOrPermit(1) +" "
    					+ (aclAnyChB.isChecked() ? "any" : makeAddress(ipOctets) +" "+ makeAddress(maskOctets))
    					+ (isLogChecked(1) ? " log" : "");
    			preCommand += "ip access-list standard "+ aclNameET.getText().toString();
    		} else {
    			command += "access-list "+ aclNumberET.getText().toString() +" "+ denyOrPermit(1) +" "
        				+ (aclAnyChB.isChecked() ? "any" : makeAddress(ipOctets) +" "+ makeAddress(maskOctets))
    					+ (isLogChecked(1) ? " log" : "");
    		}

    		Intent intent = new Intent(getActivity(), ShowCommand.class);
    		intent.putExtra("command", command);
    		if(aclNamedTypeChB.isChecked()) {
    			intent.putExtra("precommand", preCommand);
    		}

    		startActivity(intent);
    	}
    }

    /**
     * Enables to accept all IP addresses.
     * @param v Pressed check box.
     */
    public void onAnyClick(View v) {
    	switch (v.getId()) {
			case R.id.aclanyChB:
				if(aclAnyChB.isChecked()) {
					setEnabledAndFocusable(standardControls, false);
				} else {
					setEnabledAndFocusable(standardControls, true);
				}
	  			break;
    	}
    }

    /**
     * Erases the content of specific EditText(s).
     * @param v Pressed button.
     */
    public void onEraseClick(View v) {
    	switch (v.getId()) {
			case R.id.aclnumbererase:
				eraseInsertedData(aclNumberET);
				break;
			case R.id.aclsourceiperase:
				for(EditText et : sourceIPv4ControlsET) {
					eraseInsertedData(et);
				}
				for(EditText et : wildcardControlsET) {
					eraseInsertedData(et);
				}
				break;
			case R.id.aclnameerase:
				eraseInsertedData(aclNameET);
				break;
    	}
    }

    /**
     * Enables or disables inserting of ACL name.
     * @param v Pressed checkbox.
     */
    public void onNamedTypeClick(View v) {
    	CheckBox c = (CheckBox) v;
    	if(c.isChecked()) {
    		setEnabledAndFocusable(aclNameET, true);
    		setEnabledAndFocusable(aclNumberET, false);
    	} else {
    		setEnabledAndFocusable(aclNameET, false);
    		setEnabledAndFocusable(aclNumberET, true);
    	}
    }

    public void eraseInsertedData(EditText et) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
    	} else {
    		et.setTextColor(DEFAULT_TEXT_COLOR);
    	}
		et.setText("");
    }

    public void setEnabledAndFocusable(View[] controls, boolean bool) {
    	for(View control : controls) {
    		setEnabledAndFocusable(control, bool);
		}
    }

    public void setEnabledAndFocusable(View control, boolean bool) {
		control.setEnabled(bool);
    	if(!(control instanceof Spinner) && !(control instanceof RadioButton)) {
    		control.setFocusable(bool);
			control.setFocusableInTouchMode(bool);
    	}
    }

    /**
     * Differentiates whether deny or permit is selected.
     * @param mode Type of access list.
     * @return Selected value.
     */
    private String denyOrPermit(int mode) {
    	String ret = "";
    	int checked = -1;
    	if(mode == 1) {
    		checked = aclDenyPermit.getCheckedRadioButtonId();
    	}

    	switch (checked) {
    	  	case R.id.acldeny:
    	  		ret = "deny";
    	  		break;
    	  	case R.id.aclpermit:
    	  		ret = "permit";
    	  		break;
    	}

    	return ret;
    }

    /**
     * Determines whether the log is checked.
     * @param mode Type of access list.
     * @return True if the log is selected, false otherwise.
     */
    private boolean isLogChecked(int mode) {
		return aclLogChB.isChecked();
    }

    public String makeAddress(String[] octets) {
    	String ret = "";
    	for(int i = 0; i < octets.length; i++) {
    		while(octets[i].length() > 1 && octets[i].startsWith("0")) {
    			octets[i] = octets[i].substring(1);
    		}
    		ret += octets[i];
    		if(i != octets.length - 1) {
    			ret += ".";
    		}
    	}
    	return ret;
    }

    public String removeZeros(String number) {
    	while(number.length() > 1 && number.startsWith("0")) {
    		number = number.substring(1);
		}
    	return number;
    }

    public void makeAlertDialog(Set<String> alerts) {
    	String alert = "";
    	for(String s : alerts) {
    		alert += s;
    	}

    	ScrollView alertScroll = new ScrollView(getActivity().getApplicationContext());
    	TextView alertText = new TextView(getActivity().getApplicationContext());
    	alertText.setText(alert.subSequence(0, alert.length() - 2));
    	alertText.setTypeface(null, Typeface.BOLD);
    	alertText.setTextColor(Color.rgb(255, 172, 0));
    	alertText.setTextSize(16);
    	alertText.setGravity(17);
    	alertText.setPadding(4, 0, 4, 0);
    	alertScroll.addView(alertText);

    	AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
    	dialog.setIcon(android.R.drawable.ic_dialog_alert);
    	dialog.setTitle("Errors of inserted values");
    	dialog.setView(alertScroll);
    	dialog.setPositiveButton("OK", null);
    	dialog.show();
    }

    public void setVisibility(View control, int visibility) {
		control.setVisibility(visibility);
    }

    public void setLayoutBelow(View control, LayoutParams params) {
		control.setLayoutParams(params);
    }

    public boolean isIPEmpty(EditText[] octets) {
    	boolean ret = false;
    	for(EditText octet : octets) {
    		if(octet.getText().toString().replaceAll(" ", "").equals("")) {
				setError(octet, true);
				ret = true;
    		}
    	}
    	return ret;
    }

    public boolean isIPEmptyWithoutErrorWarning(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	return ret;
    }

    public void setError(View control, boolean enabled) {
    	EditText et = (EditText)control;
    	if(enabled) {
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    		et.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
    		et.setTextColor(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)));
    		et.setText(ERROR);
    	} else {
    		et.getBackground().clearColorFilter();
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
        	} else {
        		et.setTextColor(DEFAULT_TEXT_COLOR);
        	}
    		if(et.getText().toString().equals(ERROR)) {
    			et.setText("");
    		}
    	}
    }

    public boolean isAcceptedValue(String value) {
		if(value.equals(ERROR)) return false;
		return true;
	}

    public void initialization() {
    	validator = new InputDataValidation(PreferenceManager.getDefaultSharedPreferences(getActivity()));

        aclPoint1 = (TextView) layout.findViewById(R.id.aclippoint1);
        aclPoint2 = (TextView) layout.findViewById(R.id.aclippoint2);
        aclPoint3 = (TextView) layout.findViewById(R.id.aclippoint3);
        aclPoint4 = (TextView) layout.findViewById(R.id.aclippoint4);
        aclPoint5 = (TextView) layout.findViewById(R.id.aclippoint5);
        aclPoint6 = (TextView) layout.findViewById(R.id.aclippoint6);
        aclNumberET = (EditText) layout.findViewById(R.id.aclnumberET);
        aclNumberET.addTextChangedListener(new StandardACLNumberValidation());
        aclSourceIPET1 = (EditText) layout.findViewById(R.id.aclsourceipET1);
        aclSourceIPET1.addTextChangedListener(new IPv4Validation());
        aclSourceIPET2 = (EditText) layout.findViewById(R.id.aclsourceipET2);
        aclSourceIPET2.addTextChangedListener(new IPv4Validation());
        aclSourceIPET3 = (EditText) layout.findViewById(R.id.aclsourceipET3);
        aclSourceIPET3.addTextChangedListener(new IPv4Validation());
        aclSourceIPET4 = (EditText) layout.findViewById(R.id.aclsourceipET4);
        aclSourceIPET4.addTextChangedListener(new IPv4Validation());
        aclWildcardET1 = (EditText) layout.findViewById(R.id.aclwildcardET1);
        aclWildcardET1.addTextChangedListener(new IPv4Validation());
        aclWildcardET2 = (EditText) layout.findViewById(R.id.aclwildcardET2);
        aclWildcardET2.addTextChangedListener(new IPv4Validation());
        aclWildcardET3 = (EditText) layout.findViewById(R.id.aclwildcardET3);
        aclWildcardET3.addTextChangedListener(new IPv4Validation());
        aclWildcardET4 = (EditText) layout.findViewById(R.id.aclwildcardET4);
        aclWildcardET4.addTextChangedListener(new IPv4Validation());
        aclNameET = (EditText) layout.findViewById(R.id.aclnameET);

        aclDenyPermit = (RadioGroup) layout.findViewById(R.id.acldenypermit);
        aclLogChB = (CheckBox) layout.findViewById(R.id.acllogChB);
        aclAnyChB = (CheckBox) layout.findViewById(R.id.aclanyChB);
        aclAnyChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onAnyClick(v);
			}
        });

        aclNamedTypeChB = (CheckBox) layout.findViewById(R.id.aclnamedChB);
        aclNamedTypeChB.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onNamedTypeClick(v);
			}
        });

        generate = (Button) layout.findViewById(R.id.aclgenerate);
        generate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onStandardGenerateClick(v);
			}
        });

        aclNumberErase = (ImageButton) layout.findViewById(R.id.aclnumbererase);
        aclNumberErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

        aclSourceIPErase = (ImageButton) layout.findViewById(R.id.aclsourceiperase);
        aclSourceIPErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

        aclNameErase = (ImageButton) layout.findViewById(R.id.aclnameerase);
        aclNameErase.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onEraseClick(v);
			}
        });

        sourceIPv4ControlsET = new EditText[]{aclSourceIPET1, aclSourceIPET2, aclSourceIPET3,
        		aclSourceIPET4};
    	wildcardControlsET = new EditText[]{aclWildcardET1, aclWildcardET2, aclWildcardET3, aclWildcardET4};

    	standardControls = new View[]{aclPoint1, aclPoint2, aclPoint3, aclSourceIPET1,
    			aclSourceIPET2,	aclSourceIPET3, aclSourceIPET4, aclPoint4, aclPoint5, aclPoint6,
        		aclWildcardET1, aclWildcardET2, aclWildcardET3, aclWildcardET4};

    	setEnabledAndFocusable(aclNameET, false);

		SharedPreferences preferences = this.getActivity().getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("type", "acl");
		editor.commit();
    }

	/* (non-Javadoc)
	 * @see cz.muni.fi.xvyhnan.firdef.IConfiguration#makeIPv6Address(java.lang.String[])
	 */
	public String makeIPv6Address(String[] parts) {
		return null;
	}

	/* (non-Javadoc)
	 * @see cz.muni.fi.xvyhnan.firdef.IConfiguration#isIPv6Empty(android.widget.EditText[])
	 */
	public boolean isIPv6Empty(EditText[] octets) {
		return false;
	}

	/* (non-Javadoc)
	 * @see cz.muni.fi.xvyhnan.firdef.IConfiguration#onIPSelectionClick(android.view.View)
	 */
	public void onIPSelectionClick(View v) {
	}

	/* (non-Javadoc)
	 * @see cz.muni.fi.xvyhnan.firdef.IConfiguration#onOtherProtocolClick(android.view.View)
	 */
	public void onOtherProtocolClick(View v) {
	}
}
