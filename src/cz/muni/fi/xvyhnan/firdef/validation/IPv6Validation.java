/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.validation;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Validates the IPv6 part.
 * @author Jan Vyhn�nek
 */
public class IPv6Validation implements TextWatcher {
	private boolean error = false;

	public void afterTextChanged(Editable s) {
        try {
        	if(s.length() > 0) {
        		Character lastChar = s.charAt(s.length() - 1);
        		if(!Character.isDigit(lastChar)) {
        			int asciValue = lastChar;

        			if(!error && !((asciValue >= 65 && asciValue <= 70) || (asciValue >= 97 && asciValue <= 102))) {
    					s.replace(0, s.length(), s.subSequence(0, s.length() - 1));
        			}
        		}
        	}
        }
        catch(NumberFormatException nfe){}
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    /**
     * If value is true, then it enables to insert ERROR string.
     */
    public void setError(boolean value) {
    	this.error = value;
    }
}