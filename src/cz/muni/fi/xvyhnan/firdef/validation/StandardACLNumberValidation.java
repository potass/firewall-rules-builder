/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.validation;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Validates the standard ACL number.
 * @author Jan Vyhn�nek
 */
public class StandardACLNumberValidation implements TextWatcher {

	public void afterTextChanged(Editable s) {
        try {
        	if(Integer.parseInt(s.toString()) < 1) {
        		s.replace(0, s.length(), "1");
        	}
        	if(Integer.parseInt(s.toString()) > 1999) {
                s.replace(0, s.length(), "1999");
            }
        }
        catch(NumberFormatException nfe){}
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {}

}
