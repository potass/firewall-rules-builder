/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.validation;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Validates IPv4 mask.
 * @author Jan Vyhn�nek
 */
public class IPv4MaskValidation implements TextWatcher {

	public void afterTextChanged(Editable s) {
        try {
        	if(Integer.parseInt(s.toString()) > 32) {
                s.replace(0, s.length(), "32");
            }
        }
        catch(NumberFormatException nfe){}
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {}

}
