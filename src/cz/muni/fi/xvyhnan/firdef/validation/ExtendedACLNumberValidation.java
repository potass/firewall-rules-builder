/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.validation;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Validates the extended ACL number.
 * @author Jan Vyhn�nek
 */
public class ExtendedACLNumberValidation implements TextWatcher {

	public void afterTextChanged(Editable s) {
        try {
        	if(Integer.parseInt(s.toString()) < 1) {
        		s.replace(0, s.length(), "100");
        	}
        	if(Integer.parseInt(s.toString()) > 2699) {
                s.replace(0, s.length(), "2699");
            }
        }
        catch(NumberFormatException nfe){}
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    public void onTextChanged(CharSequence s, int start, int before, int count) {}

}
