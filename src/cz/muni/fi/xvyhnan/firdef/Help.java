package cz.muni.fi.xvyhnan.firdef;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.firdef.fragments.ExtendedACLFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.HelpACLFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.HelpDefinitionsFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.HelpIPtablesFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.HelpRouterOSFragment;
import cz.muni.fi.xvyhnan.firdef.fragments.StandardACLFragment;

/**
 * Shows the help.
 * @author Jan Vyhn�nek
 */
public class Help extends TabSwipeActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.activityhelptitle));

        addTab("DEFINITIONS", HelpDefinitionsFragment.class, StandardACLFragment.createBundle("DEFINITIONS"));
        addTab("CISCO ACL", HelpACLFragment.class, ExtendedACLFragment.createBundle("CISCO ACL"));
        addTab("IPTABLES", HelpIPtablesFragment.class, StandardACLFragment.createBundle("IPTABLES"));
        addTab("ROUTEROS", HelpRouterOSFragment.class, ExtendedACLFragment.createBundle("ROUTEROS"));
    }

	@Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
