/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Communicates with database.
 * @author Jan Vyhn�nek
 */
public class DBManager {
	private DBCreator dbCreator = null;
	private SQLiteDatabase db = null;

	public DBManager(Context context) {
		createDB(context);
	}

	/**
	 * Creates database.
	 * @param context Current context.
	 * @return Created or existing database.
	 */
	private SQLiteDatabase createDB(Context context) {
		dbCreator = new DBCreator(context);
		db = dbCreator.getWritableDatabase();
		dbCreator.onCreate(db);
		return db;
	}

	/**
	 * Returns database.
	 * @return Current database.
	 */
	public SQLiteDatabase getDB() {
		return db;
	}

	/**
	 * Resets database.
	 */
	public void resetDB() {
		dbCreator.onUpgrade(db, 0, 0);
		dbCreator.onCreate(db);
	}

	/**
	 * Closes connection with database.
	 */
	public void closeDB() {
		if(db.isOpen()) {
			db.close();
		}
	}
}
