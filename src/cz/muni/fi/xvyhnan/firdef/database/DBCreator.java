/**
 *
 */
package cz.muni.fi.xvyhnan.firdef.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Creates and updates database tables.
 * @author Jan Vyhn�nek
 */
public class DBCreator extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "Firewall Builder";
	private static final String TABLE_NAME = "Rules";
	private static final String COLUMN1 = "Rule";
	private static final String COLUMN2 = "PreRule";
	private static final String COLUMN3 = "Type";
	private static final String COLUMN4 = "Inserted";

	public DBCreator(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String str = "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME +" (id INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN1 +" TEXT NOT NULL, "+ COLUMN2 +" TEXT NULL, "+ COLUMN3 +" TEXT(20) NOT NULL, "+ COLUMN4 +" TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
		db.execSQL(str);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String str = "DROP TABLE IF EXISTS "+ TABLE_NAME;
		db.execSQL(str);
		onCreate(db);
	}
}
