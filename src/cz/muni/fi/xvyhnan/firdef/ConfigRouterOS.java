package cz.muni.fi.xvyhnan.firdef;

import java.util.HashSet;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.firdef.validation.IPv4MaskValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv4Validation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6MaskValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6Validation;
import cz.muni.fi.xvyhnan.firdef.validation.PortValidation;

/**
 * Shows configuration window of RouterOS.
 * @author Jan Vyhn�nek
 */
public class ConfigRouterOS extends SherlockActivity implements IConfiguration {
	private final String ERROR = "!!!";
	private final int DEFAULT_TEXT_COLOR = Color.WHITE;
	private final int DEFAULT_TEXT_COLOR_WHITE = Color.BLACK;
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private final boolean DEFAULT_VALIDATION = true;
	private InputDataValidation validator;
	private IPv6Validation ipv6Validation;
	private int selectedChain = 0, selectedAction = 0, selectedState = 0;
	private TextView chain;
	private EditText otherChainET, sourceIPET1, sourceIPET2, sourceIPET3, sourceIPET4, destinationIPET1,
			destinationIPET2, destinationIPET3, destinationIPET4, sourceMaskET, sourcePortET,
			destinationMaskET, destinationPortET, otherProtocolET, inInterfaceET, outInterfaceET, sourceIPv6ET1,
			sourceIPv6ET2, sourceIPv6ET3, sourceIPv6ET4, sourceIPv6ET5, sourceIPv6ET6, sourceIPv6ET7,
			sourceIPv6ET8, destinationIPv6ET1, destinationIPv6ET2, destinationIPv6ET3, destinationIPv6ET4,
			destinationIPv6ET5, destinationIPv6ET6, destinationIPv6ET7, destinationIPv6ET8;
	private Spinner chains, actions, states, protocols;
	private CheckBox otherChainChB, otherProtocolChB;
	private View[] chainControls, otherChainControls;
	private EditText[] sourceIPv4Controls, destinationIPv4Controls, sourceIPv6Controls,
			destinationIPv6Controls;
	private RadioGroup ipv4Ipv6;
	private RelativeLayout sourceIPv4RL, destinationIPv4RL, sourceIPv6RL, destinationIPv6RL, sourceMaskPortHeader,
			destinationIPHeader;
	private LayoutParams destinationIPP, sourceMaskPortP;
	private TextWatcher watcher1, watcher2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setContentView(R.layout.configrouteros);
        setTitle(getString(R.string.activityrouterostitle));

        addListenerOnChainsItemSelection();
        addListenerOnActionsItemSelection();
        addListenerOnStatesItemSelection();

        initialization();
    }

    @Override
    public void onResume() {
    	super.onResume();

    	for(EditText e : sourceIPv4Controls) {
			setError(e, false);
		}
		for(EditText e : destinationIPv4Controls) {
			setError(e, false);
		}
		for(EditText e : sourceIPv6Controls) {
			setError(e, false);
		}
		for(EditText e : destinationIPv6Controls) {
			setError(e, false);
		}

		setError(otherChainET, false);
		setError(otherProtocolET, false);
		setError(sourceMaskET, false);
		setError(destinationMaskET, false);
		setError(sourcePortET, false);
		setError(destinationPortET, false);
    }

    /**
     * Adds listener on the chain selection spinner.
     */
    private void addListenerOnChainsItemSelection() {
    	chains = (Spinner) findViewById(R.id.typeofchain);
    	chains.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 0) {
    	    		selectedChain = 0;
    	    	} else if(position == 1) {
    	    		selectedChain = 1;
    	    	} else {
    	    		selectedChain = 2;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedChain = 0;
    	    }
    	});
    }

    /**
     * Adds listener on the action selection spinner.
     */
    private void addListenerOnActionsItemSelection() {
    	actions = (Spinner) findViewById(R.id.typeofaction);
    	actions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 0) {
    	    		selectedAction = 0;
    	    	} else if(position == 1) {
    	    		selectedAction = 1;
    	    	} else {
    	    		selectedAction = 2;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedAction = 0;
    	    }
    	});
    }

    /**
     * Adds listener on the state selection spinner.
     */
    private void addListenerOnStatesItemSelection() {
    	states = (Spinner) findViewById(R.id.typeofstate);
    	states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 0) {
    	    		selectedState = 0;
    	    	} else if(position == 1) {
    	    		selectedState = 1;
    	    	} else if(position == 2) {
    	    		selectedState = 2;
    	    	} else if(position == 3) {
    	    		selectedState = 3;
    	    	} else {
    	    		selectedState = 4;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedState = 0;
    	    }
    	});
    }

    public void onIPSelectionClick(View v) {
    	int checkedIP = ipv4Ipv6.getCheckedRadioButtonId();
    	if(checkedIP == R.id.rosipv4) {
    		destinationIPP.addRule(RelativeLayout.BELOW, R.id.rossourceipRL);
    		sourceMaskPortP.addRule(RelativeLayout.BELOW, R.id.rosdestinationipRL);

    		setLayoutBelow(destinationIPHeader, destinationIPP);
    		setLayoutBelow(sourceMaskPortHeader, sourceMaskPortP);

    		sourceMaskET.setText("");
    		sourceMaskET.setHint(R.string.hmask);
    		sourceMaskET.removeTextChangedListener(watcher2);
    		sourceMaskET.addTextChangedListener(watcher1);
    		destinationMaskET.setText("");
    		destinationMaskET.setHint(R.string.hmask);
    		destinationMaskET.removeTextChangedListener(watcher2);
    		destinationMaskET.addTextChangedListener(watcher1);

			setVisibility(sourceIPv4RL, 0);
			setVisibility(destinationIPv4RL, 0);
			setVisibility(sourceIPv6RL, 4);
			setVisibility(destinationIPv6RL, 4);
    	} else {
    		destinationIPP.addRule(RelativeLayout.BELOW, R.id.rossourceipv6RL);
    		sourceMaskPortP.addRule(RelativeLayout.BELOW, R.id.rosdestinationipv6RL);

    		setLayoutBelow(destinationIPHeader, destinationIPP);
    		setLayoutBelow(sourceMaskPortHeader, sourceMaskPortP);

    		sourceMaskET.setText("");
    		sourceMaskET.setHint(R.string.hmask2);
    		sourceMaskET.removeTextChangedListener(watcher1);
    		sourceMaskET.addTextChangedListener(watcher2);
    		destinationMaskET.setText("");
    		destinationMaskET.setHint(R.string.hmask2);
    		destinationMaskET.removeTextChangedListener(watcher1);
    		destinationMaskET.addTextChangedListener(watcher2);

    		setVisibility(sourceIPv4RL, 4);
    		setVisibility(destinationIPv4RL, 4);
    		setVisibility(sourceIPv6RL, 0);
			setVisibility(destinationIPv6RL, 0);
    	}
    }

    /**
     * Enable to insert the other chain parameter.
     * @param v Pressed check box.
     */
    public void onOtherChainClick(View v) {
    	if(otherChainChB.isChecked()) {
    		setEnabledAndFocusable(chainControls, false);
    		setEnabledAndFocusable(otherChainControls, true);
    	} else {
    		setEnabledAndFocusable(chainControls, true);
    		setEnabledAndFocusable(otherChainControls, false);
    	}
    }

    public void onOtherProtocolClick(View v) {
    	if(otherProtocolChB.isChecked()) {
    		setEnabledAndFocusable(otherProtocolET, true);
    		setEnabledAndFocusable(protocols, false);
    	} else {
    		setEnabledAndFocusable(otherProtocolET, false);
    		setEnabledAndFocusable(protocols, true);
    	}
    }

    /**
     * Validates inserted values, generates the filtering rule and shows it.
     * @param v Pressed button.
     */
    public void onGenerateClick(View v) {
    	boolean error = false;

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);

    	Set<String> alert = new HashSet<String>();

    	Set<EditText> errorControls = new HashSet<EditText>();

    	String[] sourceIPAddress = new String[]{sourceIPET1.getText().toString(),
				sourceIPET2.getText().toString(), sourceIPET3.getText().toString(),
				sourceIPET4.getText().toString()};
		String[] destinationIPAddress = new String[]{destinationIPET1.getText().toString(),
				destinationIPET2.getText().toString(), destinationIPET3.getText().toString(),
    			destinationIPET4.getText().toString()};
		String[] sourceIPv6Address = new String[]{sourceIPv6ET1.getText().toString(),
				sourceIPv6ET2.getText().toString(), sourceIPv6ET3.getText().toString(),
				sourceIPv6ET4.getText().toString(), sourceIPv6ET5.getText().toString(),
				sourceIPv6ET6.getText().toString(), sourceIPv6ET7.getText().toString(),
				sourceIPv6ET8.getText().toString()};
		String[] destinationIPv6Address = new String[]{destinationIPv6ET1.getText().toString(),
				destinationIPv6ET2.getText().toString(), destinationIPv6ET3.getText().toString(),
				destinationIPv6ET4.getText().toString(), destinationIPv6ET5.getText().toString(),
				destinationIPv6ET6.getText().toString(), destinationIPv6ET7.getText().toString(),
				destinationIPv6ET8.getText().toString()};

		if(colorPreferences.getBoolean("validation", DEFAULT_VALIDATION)) {
			ipv6Validation.setError(true);

			setError(otherChainET, false);
			if (otherChainChB.isChecked()
					&& (otherChainET.getText().toString().replaceAll(" ", "")
							.equals("") || !isAcceptedValue(otherChainET
							.getText().toString()))) {
				alert.add(getString(R.string.missingotherchain) + "\n\n");
				setError(otherChainET, true);
				errorControls.add(otherChainET);
				error = true;
			}

			setError(otherProtocolET, false);
			if (otherProtocolChB.isChecked()
					&& (otherProtocolET.getText().toString()
							.replaceAll(" ", "").equals("") || !isAcceptedValue(otherProtocolET
							.getText().toString()))) {
				alert.add(getString(R.string.missingotherprotocol) + "\n\n");
				setError(otherProtocolET, true);
				errorControls.add(otherProtocolET);
				error = true;
			}

			for (EditText e : sourceIPv4Controls) {
				setError(e, false);
			}
			for (EditText e : destinationIPv4Controls) {
				setError(e, false);
			}
			for (EditText e : sourceIPv6Controls) {
				setError(e, false);
			}
			for (EditText e : destinationIPv6Controls) {
				setError(e, false);
			}

			setError(sourceMaskET, false);
			setError(destinationMaskET, false);

			if (ipv4Ipv6.getCheckedRadioButtonId() == R.id.rosipv4) {
				if (!isIPEmptyWithoutErrorWarning(sourceIPv4Controls)
						&& !validator.isIpAddress(sourceIPv4Controls)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}

				if (!isIPEmptyWithoutErrorWarning(destinationIPv4Controls)
						&& !validator.isIpAddress(destinationIPv4Controls)) {
					alert.add(getString(R.string.wrongsourceip) + "\n\n");
					error = true;
				}

				if (!sourceMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isMaskCorrect(sourceMaskET)) {
					alert.add(getString(R.string.wrongmask) + "\n\n");
					error = true;
				}

				if (!sourceMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& isIPEmpty(sourceIPv4Controls)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}

				if (!destinationMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isMaskCorrect(destinationMaskET)) {
					alert.add(getString(R.string.wrongmask) + "\n\n");
					error = true;
				}

				if (!destinationMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& isIPEmpty(destinationIPv4Controls)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}
			} else {
				if (!validator.isIPv6Address(sourceIPv6Controls)) {
					alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
					error = true;
				}

				if (!validator.isIPv6Address(destinationIPv6Controls)) {
					alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
					error = true;
				}

				if (!sourceMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isIPv6MaskCorrect(sourceMaskET)) {
					alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
					error = true;
				}

				if (!sourceMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& isIPv6Empty(sourceIPv6Controls)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}

				if (!destinationMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isIPv6MaskCorrect(destinationMaskET)) {
					alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
					error = true;
				}

				if (!destinationMaskET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& isIPv6Empty(destinationIPv6Controls)) {
					alert.add(getString(R.string.missingip) + "\n\n");
					error = true;
				}
			}

			setError(sourcePortET, false);
			if (!sourcePortET.getText().toString().replaceAll(" ", "")
					.equals("")
					&& !validator.isPortCorrect(sourcePortET)) {
				alert.add(getString(R.string.wrongport) + "\n\n");
				error = true;
			}

			setError(destinationPortET, false);
			if (!destinationPortET.getText().toString().replaceAll(" ", "")
					.equals("")
					&& !validator.isPortCorrect(destinationPortET)) {
				alert.add(getString(R.string.wrongport) + "\n\n");
				error = true;
			}

			ipv6Validation.setError(false);
		}

		if(error) {
			makeAlertDialog(alert);
		} else {
			String command = "add chain=";
			if(otherChainChB.isChecked()) {
    			command += otherChainET.getText().toString();
    		} else {
    			if(selectedChain == 0) {
        			command += "input";
        		} else if(selectedChain == 1) {
        			command += "output";
        		} else {
        			command += "forward";
        		}
    		}

			command += " action=";
			if(selectedAction == 0) {
    			command += "accept";
    		} else if(selectedAction == 1) {
    			command += "drop";
    		} else {
    			command += "log";
    		}

			if(ipv4Ipv6.getCheckedRadioButtonId() == R.id.rosipv4) {
				if(!isIPEmptyWithoutErrorWarning(sourceIPv4Controls)) {
    				command += " src-address="+ makeAddress(sourceIPAddress);
    				if(!sourceMaskET.getText().toString().replaceAll(" ", "").equals("")) {
    					command += "/"+ removeZeros(sourceMaskET.getText().toString());
    				}
    			}

				if(!isIPEmptyWithoutErrorWarning(destinationIPv4Controls)) {
    				command += " dst-address="+ makeAddress(destinationIPAddress);
    				if(!destinationMaskET.getText().toString().replaceAll(" ", "").equals("")) {
    					command += "/"+ removeZeros(destinationMaskET.getText().toString());
    				}
    			}
			} else {
				if(!isIPEmptyWithoutErrorWarning(sourceIPv6Controls)) {
					command += " src-address="+ makeIPv6Address(sourceIPv6Address);
					if(!sourceMaskET.getText().toString().replaceAll(" ", "").equals("")) {
						command += "/"+ removeZeros(sourceMaskET.getText().toString());
					}
				}

				if(!isIPEmptyWithoutErrorWarning(destinationIPv6Controls)) {
					command += " dst-address="+ makeIPv6Address(destinationIPv6Address);
					if(!destinationMaskET.getText().toString().replaceAll(" ", "").equals("")) {
						command += "/"+ removeZeros(destinationMaskET.getText().toString());
					}
				}
			}

			if(!sourcePortET.getText().toString().replaceAll(" ", "").equals("")) {
				command += " src-port="+ removeZeros(sourcePortET.getText().toString());
			}

			if(!destinationPortET.getText().toString().replaceAll(" ", "").equals("")) {
				command += " dst-port="+ removeZeros(destinationPortET.getText().toString());
			}

			if(!inInterfaceET.getText().toString().replaceAll(" ", "").equals("")) {
    			command += " in-interface="+ inInterfaceET.getText().toString();
    		}

    		if(!outInterfaceET.getText().toString().replaceAll(" ", "").equals("")) {
    			command += " out-interface="+ outInterfaceET.getText().toString();
    		}

    		if(otherProtocolChB.isChecked() && !otherProtocolET.getText().toString().replaceAll(" ", "").equals("")) {
    			command += " protocol="+ otherProtocolET.getText().toString();
    		} else {
    			String selectedProtocol = protocols.getSelectedItem().toString();
    			if(!selectedProtocol.equals("none")) {
    				command += " protocol="+ selectedProtocol;
    			}
    		}

    		if(selectedState == 1) {
	    		command += " connection-state=new";
	    	} else if(selectedState == 2) {
	    		command += " connection-state=established";
	    	} else if(selectedState == 3) {
	    		command += " connection-state=related";
	    	} else if(selectedState == 4){
	    		command += " connection-state=invalid";
	    	}

			Intent intent = new Intent(this, ShowCommand.class);
    		intent.putExtra("command", command);
    		startActivity(intent);
		}
    }

    /**
     * Erases the content of specific EditText(s).
     * @param v Pressed button.
     */
    public void onEraseClick(View v) {
    	int checkedIP;
    	switch (v.getId()) {
    	case R.id.roschainerase:
			eraseInsertedData(otherChainET);
			break;
    	case R.id.rossourceiperase:
    		checkedIP = ipv4Ipv6.getCheckedRadioButtonId();
    		if(checkedIP == R.id.rosipv4) {
				for (EditText et : sourceIPv4Controls) {
					eraseInsertedData(et);
				}
			} else {
				for (EditText et : sourceIPv6Controls) {
					eraseInsertedData(et);
				}
			}
			break;
		case R.id.rosdestinationiperase:
			checkedIP = ipv4Ipv6.getCheckedRadioButtonId();
			if(checkedIP == R.id.rosipv4) {
				for (EditText et : destinationIPv4Controls) {
					eraseInsertedData(et);
				}
			} else {
				for (EditText et : destinationIPv6Controls) {
					eraseInsertedData(et);
				}
			}
			break;
		case R.id.rossourcemaskporterase:
			eraseInsertedData(sourceMaskET);
			eraseInsertedData(sourcePortET);
			break;
		case R.id.rosdestinationmaskporterase:
			eraseInsertedData(destinationMaskET);
			eraseInsertedData(destinationPortET);
			break;
		case R.id.rosininterfaceerase:
			eraseInsertedData(inInterfaceET);
			break;
		case R.id.rosoutinterfaceerase:
			eraseInsertedData(outInterfaceET);
			break;
		case R.id.rosprotocolerase:
			eraseInsertedData(otherProtocolET);
			break;
    	}
    }

    public void eraseInsertedData(EditText et) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
    	} else {
    		et.setTextColor(DEFAULT_TEXT_COLOR);
    	}
		et.setText("");
    }

    public void setEnabledAndFocusable(View[] controls, boolean bool) {
    	for(View control : controls) {
    		setEnabledAndFocusable(control, bool);
		}
    }

    public void setEnabledAndFocusable(View control, boolean bool) {
    	control.setEnabled(bool);
    	if(!(control instanceof CheckBox) && !(control instanceof Spinner)) {
    		control.setFocusable(bool);
			control.setFocusableInTouchMode(bool);
    	}
    }

    public String makeAddress(String[] octets) {
    	String ret = "";
    	for(int i = 0; i < octets.length; i++) {
    		while(octets[i].length() > 1 && octets[i].startsWith("0")) {
    			octets[i] = octets[i].substring(1);
    		}
    		ret += octets[i];
    		if(i != octets.length - 1) {
    			ret += ".";
    		}
    	}
    	return ret;
    }

    public String makeIPv6Address(String[] parts) {
    	for(int i = 0; i < parts.length; i++) {
    		if(parts[i].length() > 0) {
    			while(parts[i].length() > 1 && parts[i].startsWith("0")) {
    				parts[i] = parts[i].substring(1);
    			}
    		} else {
    			parts[i] = "0";
    		}
    	}

    	int begin = -1, end = -1;
    	for(int i = 0; i < parts.length - 1; i++) {
    		if(parts[i].equals("0") && parts[i + 1].equals("0")) {
    			if(begin == -1) {
    				begin = i;
    			}
    			end = i + 1;
    		} else if(i > 0 && parts[i - 1].equals("0") && parts[i].equals("0") && !parts[i + 1].equals("0")) {
    			break;
    		}
    	}

    	String ret = "";
    	for(int i = 0; i < parts.length; i++) {
    		if(i == begin || i == end) {
    			ret += ":";
    		} else if(i < begin || i > end) {
    			ret += parts[i].toUpperCase();
        		if(i != parts.length - 1) {
        			ret += ":";
        		}
    		}
    	}
    	ret = ret.replace(":::", "::");

    	return ret;
    }

    public String removeZeros(String number) {
    	while(number.length() > 1 && number.startsWith("0")) {
    		number = number.substring(1);
		}
    	return number;
    }

    public void makeAlertDialog(Set<String> alerts) {
    	String alert = "";
    	for(String s : alerts) {
    		alert += s;
    	}

    	ScrollView alertScroll = new ScrollView(getApplicationContext());
    	TextView alertText = new TextView(getApplicationContext());
    	alertText.setText(alert.subSequence(0, alert.length() - 2));
    	alertText.setTypeface(null, Typeface.BOLD);
    	alertText.setTextColor(Color.rgb(255, 172, 0));
    	alertText.setTextSize(16);
    	alertText.setGravity(17);
    	alertText.setPadding(4, 0, 4, 0);
    	alertScroll.addView(alertText);

    	AlertDialog.Builder dialog = new AlertDialog.Builder(this);
    	dialog.setIcon(android.R.drawable.ic_dialog_alert);
    	dialog.setTitle("Errors of inserted values");
    	dialog.setView(alertScroll);
    	dialog.setPositiveButton("OK", null);
    	dialog.show();
    }

    public boolean isIPEmpty(EditText[] octets) {
    	boolean ret = false;
    	for(EditText octet : octets) {
    		if(octet.getText().toString().replaceAll(" ", "").equals("")) {
				setError(octet, true);
				ret = true;
    		}
    	}
    	return ret;
    }

    public boolean isIPv6Empty(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	if(ret) {
    		for(EditText octet : octets) {
        		setError(octet, true);
    		}
    	}
    	return ret;
    }

    public boolean isIPEmptyWithoutErrorWarning(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	return ret;
    }

    public void setVisibility(View control, int visibility) {
		control.setVisibility(visibility);
    }

    public void setLayoutBelow(View control, LayoutParams params) {
		control.setLayoutParams(params);
    }

    public void setError(View control, boolean enabled) {
    	EditText et = (EditText)control;
    	if(enabled) {
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    		et.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
    		et.setTextColor(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)));
    		et.setText(ERROR);
    	} else {
    		et.getBackground().clearColorFilter();
    		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
        	} else {
        		et.setTextColor(DEFAULT_TEXT_COLOR);
        	}
    		if(et.getText().toString().equals(ERROR)) {
    			et.setText("");
    		}
    	}
    }

    public boolean isAcceptedValue(String value) {
		if(value.equals(ERROR)) return false;
		return true;
	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuItem help = menu.add("Help");
    	help.setIcon(android.R.drawable.ic_menu_help);
    	help.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    	help.setIntent(new Intent(this, Help.class));
		return true;
	}

    public void initialization() {
    	validator = new InputDataValidation(PreferenceManager.getDefaultSharedPreferences(this));

    	watcher1 = new IPv4MaskValidation();
    	watcher2 = new IPv6MaskValidation();

    	protocols = (Spinner) findViewById(R.id.rostypeofprotocol);
    	chain = (TextView) findViewById(R.id.roschain);
    	otherChainET = (EditText) findViewById(R.id.rosotherchainET);
    	sourceIPET1 = (EditText) findViewById(R.id.rossourceipET1);
    	sourceIPET1.addTextChangedListener(new IPv4Validation());
    	sourceIPET2 = (EditText) findViewById(R.id.rossourceipET2);
    	sourceIPET2.addTextChangedListener(new IPv4Validation());
    	sourceIPET3 = (EditText) findViewById(R.id.rossourceipET3);
    	sourceIPET3.addTextChangedListener(new IPv4Validation());
    	sourceIPET4 = (EditText) findViewById(R.id.rossourceipET4);
    	sourceIPET4.addTextChangedListener(new IPv4Validation());

    	ipv6Validation = new IPv6Validation();

    	sourceIPv6ET1 = (EditText) findViewById(R.id.rossourceipv6ET1);
    	sourceIPv6ET1.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET2 = (EditText) findViewById(R.id.rossourceipv6ET2);
    	sourceIPv6ET2.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET3 = (EditText) findViewById(R.id.rossourceipv6ET3);
    	sourceIPv6ET3.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET4 = (EditText) findViewById(R.id.rossourceipv6ET4);
    	sourceIPv6ET4.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET5 = (EditText) findViewById(R.id.rossourceipv6ET5);
    	sourceIPv6ET5.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET6 = (EditText) findViewById(R.id.rossourceipv6ET6);
    	sourceIPv6ET6.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET7 = (EditText) findViewById(R.id.rossourceipv6ET7);
    	sourceIPv6ET7.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET8 = (EditText) findViewById(R.id.rossourceipv6ET8);
    	sourceIPv6ET8.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET1 = (EditText) findViewById(R.id.rosdestinationipv6ET1);
    	destinationIPv6ET1.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET2 = (EditText) findViewById(R.id.rosdestinationipv6ET2);
    	destinationIPv6ET2.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET3 = (EditText) findViewById(R.id.rosdestinationipv6ET3);
    	destinationIPv6ET3.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET4 = (EditText) findViewById(R.id.rosdestinationipv6ET4);
    	destinationIPv6ET4.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET5 = (EditText) findViewById(R.id.rosdestinationipv6ET5);
    	destinationIPv6ET5.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET6 = (EditText) findViewById(R.id.rosdestinationipv6ET6);
    	destinationIPv6ET6.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET7 = (EditText) findViewById(R.id.rosdestinationipv6ET7);
    	destinationIPv6ET7.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET8 = (EditText) findViewById(R.id.rosdestinationipv6ET8);
    	destinationIPv6ET8.addTextChangedListener(ipv6Validation);
    	destinationIPET1 = (EditText) findViewById(R.id.rosdestinationipET1);
    	destinationIPET1.addTextChangedListener(new IPv4Validation());
    	destinationIPET2 = (EditText) findViewById(R.id.rosdestinationipET2);
    	destinationIPET2.addTextChangedListener(new IPv4Validation());
    	destinationIPET3 = (EditText) findViewById(R.id.rosdestinationipET3);
    	destinationIPET3.addTextChangedListener(new IPv4Validation());
    	destinationIPET4 = (EditText) findViewById(R.id.rosdestinationipET4);
    	destinationIPET4.addTextChangedListener(new IPv4Validation());
    	sourceMaskET = (EditText) findViewById(R.id.rossourcemaskET);
    	sourceMaskET.addTextChangedListener(watcher1);
    	sourcePortET = (EditText) findViewById(R.id.rossourceportET);
    	sourcePortET.addTextChangedListener(new PortValidation());
    	destinationMaskET = (EditText) findViewById(R.id.rosdestinationmaskET);
    	destinationMaskET.addTextChangedListener(watcher1);
    	destinationPortET = (EditText) findViewById(R.id.rosdestinationportET);
    	destinationPortET.addTextChangedListener(new PortValidation());
    	otherProtocolET = (EditText) findViewById(R.id.rosotherprotocolET);
    	inInterfaceET = (EditText) findViewById(R.id.rosininterfaceET);
    	outInterfaceET = (EditText) findViewById(R.id.rosoutinterfaceET);
    	otherChainChB = (CheckBox) findViewById(R.id.rosotherchainChB);
    	otherProtocolChB = (CheckBox) findViewById(R.id.rosotherprotocolChB);
    	ipv4Ipv6 = (RadioGroup) findViewById(R.id.rosip4ip6);
    	sourceIPv4RL = (RelativeLayout) findViewById(R.id.rossourceipRL);
    	destinationIPv4RL = (RelativeLayout) findViewById(R.id.rosdestinationipRL);
    	sourceIPv6RL = (RelativeLayout) findViewById(R.id.rossourceipv6RL);
    	destinationIPv6RL = (RelativeLayout) findViewById(R.id.rosdestinationipv6RL);
    	sourceMaskPortHeader = (RelativeLayout) findViewById(R.id.rossourcemaskportRLH);
    	destinationIPHeader = (RelativeLayout) findViewById(R.id.rosdestinationipRLH);

    	destinationIPP = (LayoutParams) destinationIPHeader.getLayoutParams();
    	sourceMaskPortP = (LayoutParams) sourceMaskPortHeader.getLayoutParams();

    	sourceIPv4Controls = new EditText[]{sourceIPET1, sourceIPET2, sourceIPET3, sourceIPET4};
    	destinationIPv4Controls = new EditText[]{destinationIPET1, destinationIPET2, destinationIPET3,
    			destinationIPET4};
    	sourceIPv6Controls = new EditText[]{sourceIPv6ET1, sourceIPv6ET2, sourceIPv6ET3, sourceIPv6ET4,
    			sourceIPv6ET5, sourceIPv6ET6, sourceIPv6ET7, sourceIPv6ET8};
    	destinationIPv6Controls = new EditText[]{destinationIPv6ET1, destinationIPv6ET2, destinationIPv6ET3,
    			destinationIPv6ET4, destinationIPv6ET5, destinationIPv6ET6, destinationIPv6ET7,
    			destinationIPv6ET8};

    	chainControls = new View[]{chain, chains};
    	otherChainControls = new View[]{otherChainET};

    	setEnabledAndFocusable(otherChainControls, false);
    	setEnabledAndFocusable(otherProtocolET, false);

    	SharedPreferences preferences = getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("type", "ros");
		editor.commit();
    }

    @Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
