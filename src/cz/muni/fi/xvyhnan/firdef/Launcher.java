package cz.muni.fi.xvyhnan.firdef;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Launch the whole application.
 * @author Jan Vyhn�nek
 */
public class Launcher extends SherlockActivity {
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private Spinner technologies;
	private int selectedTechnology = 0;
	private String theme = "";
	private boolean reset = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	this.theme = colorPreferences.getString("apptheme", "Black");
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher);

        addListenerOnTechnologiesItemSelection();

        SharedPreferences preferences = getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("precommand", "");
		editor.commit();

		SharedPreferences.Editor editor2 = colorPreferences.edit();
		if(!colorPreferences.contains("validation")) {
			editor2.putBoolean("validation", true);
		}
		if(!colorPreferences.contains("apptheme")) {
			editor2.putString("apptheme", "Black");
		}
		if(!colorPreferences.contains("errorscolor")) {
			editor2.putString("errorscolor", DEFAULT_ERROR_COLOR);
		}
		editor2.commit();

        showHelpOnFirstLaunch();
        showInternetInfo();
    }

    @Override
    public void onResume() {
    	super.onResume();

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);

    	if(!colorPreferences.getString("apptheme", "Black").equals(theme) || this.reset) {
    		Intent intent = getIntent();
        	finish();
        	startActivity(intent);
    	}
    }

    @Override
    public void onPause() {
    	super.onPause();

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	this.theme = colorPreferences.getString("apptheme", "Black");
    }

    @Override
    public void onDestroy() {
    	super.onDestroy();

    	if(isFinishing()) {
    		SharedPreferences preferences = getSharedPreferences("preferences", 0);
    		SharedPreferences.Editor editor = preferences.edit();
    		editor.putBoolean("internetinfo", true);
    		editor.commit();
    	}
    }

    /**
     * Adds listener on the technology selection spinner.
     */
    private void addListenerOnTechnologiesItemSelection() {
    	technologies = (Spinner) findViewById(R.id.technology);
    	technologies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()	{
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	selectedTechnology = position;
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedTechnology = 0;
    	    }
    	});
    }

    /**
     * Confirms selection of the technology.
     * @param v Pressed button.
     */
    public void onConfirmClick(View v) {
    	if(selectedTechnology == 0) {
    		startActivity(new Intent(this, ConfigCiscoACL.class));
    	} else if(selectedTechnology == 1) {
    		startActivity(new Intent(this, ConfigIPtables.class));
    	} else {
    		startActivity(new Intent(this, ConfigRouterOS.class));
    	}
    }

    /**
     * Shows the tutorial.
     * @param v Pressed button.
     */
    public void onTutorialClick(View v) {
    	startActivity(new Intent(this, Tutorial.class));
    }

    /**
     * Shows the help.
     * @param v Pressed button.
     */
    public void onHelpClick(View v) {
    	startActivity(new Intent(this, Help.class));
    }

    /**
     * Shows dialog which asks user if he wants to open the help or the tutorial. Only when the application is first launched.
     */
    private void showHelpOnFirstLaunch() {
    	SharedPreferences preferences = getSharedPreferences("preferences", 0);
    	SharedPreferences.Editor editor2 = preferences.edit();

    	if(!preferences.contains("firstlaunch")) {
        	editor2.putBoolean("firstlaunch", true);
        	editor2.commit();
    	}

        if(preferences.getBoolean("firstlaunch", false)) {
        	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(Launcher.this);
    		SharedPreferences.Editor editor = colorPreferences.edit();

    		editor.putString("errorscolor", DEFAULT_ERROR_COLOR);
    		editor.putString("apptheme", "Black");
    		editor.putBoolean("validation", true);
    		editor.commit();

        	new AlertDialog.Builder(Launcher.this)
			.setMessage(R.string.firsttimehelp)
			.setPositiveButton(R.string.tutorial, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					onTutorialClick(null);
				}
			})
			.setNeutralButton(R.string.help, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					onHelpClick(null);
				}
			})
			.setNegativeButton("Cancel", null).show();

        	editor2.putBoolean("firstlaunch", false);
        	editor2.commit();
        }
    }

    /**
     * Shows dialog which informs user that the device is not connected to the network.
     */
    private void showInternetInfo() {
    	SharedPreferences preferences = getSharedPreferences("preferences", 0);
    	SharedPreferences.Editor editor = preferences.edit();

    	if(!preferences.contains("internetinfo")) {
        	editor.putBoolean("internetinfo", true);
        	editor.commit();
    	}

    	if(preferences.getBoolean("internetinfo", false)) {
    		if(!isConnected(this)) {
    			new AlertDialog.Builder(Launcher.this)
    					.setTitle(R.string.alertnoconnetiontitle)
    					.setMessage(R.string.alertnoconnetion)
    					.setIcon(R.drawable.firewall2)
    					.setPositiveButton("OK", null).show();

    			editor.putBoolean("internetinfo", false);
            	editor.commit();
    		}
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.launchermenu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.tutorialmenu:
			startActivity(new Intent(this, Tutorial.class));
			return true;
		case R.id.helpmenu:
			startActivity(new Intent(this, Help.class));
			return true;
		case R.id.actionmodemenu:
			startActionMode(new AnActionModeOfEpicProportions());
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Tries to start the given intent.
	 *
	 * @param intent Intent which is going to start
	 * @return True if intent is started, false otherwise
	 */
	private boolean MyStartActivity(Intent intent) {
	    try {
	        startActivity(intent);
	        return true;
	    } catch (ActivityNotFoundException e) {
	        return false;
	    }
	}

	/**
	 * Determines whether there is a connection to some network.
	 *
	 * @param context Context on which is connection searched
	 * @return true if the device is connected to some network, false otherwise
	 */
	private static boolean isConnected(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = null;
		if (connectivityManager != null) {
			networkInfo = connectivityManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		}
		return (networkInfo == null) ? false : networkInfo.isConnected();
	}

	@Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}

	/**
	 * Handles action bar overflow menu.
	 */
	private final class AnActionModeOfEpicProportions implements ActionMode.Callback {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        	MenuInflater inflater = getSupportMenuInflater();
    		inflater.inflate(R.menu.launcheractionmodemenu, menu);
    		return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			mode.finish();
            return prepareOnActionModeClickMenu(item);
        }

        public void onDestroyActionMode(ActionMode mode) {
        }
	}

	/**
	 * Reacts when menu item is pressed.
	 * @param item Selected menu item.
	 */
	public boolean prepareOnActionModeClickMenu(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rateapp:
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri
					.parse("http://market.android.com/search?q=pname:cz.muni.fi.xvyhnan.firdef"));
			if (MyStartActivity(intent) == false) {
				intent.setData(Uri
						.parse("https://play.google.com/store/apps/details?cz.muni.fi.xvyhnan.firdef"));
				if (MyStartActivity(intent) == false) {
					Toast.makeText(
							this,
							"Could not open Google Play, please install the market app.",
							Toast.LENGTH_SHORT).show();
				}
			}
			return true;
		case R.id.colorsettings:
			startActivity(new Intent(this, ColorSettings.class));
			return true;
		case R.id.colorsettingsdefault:
			try {
				new AlertDialog.Builder(this)
						.setMessage(R.string.alertrestorecolorsettings)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialog,
											int which) {
										SharedPreferences colorPreferences = PreferenceManager
												.getDefaultSharedPreferences(Launcher.this);
										SharedPreferences.Editor editor = colorPreferences
												.edit();

										editor.putString("errorscolor",
												DEFAULT_ERROR_COLOR);
										editor.putString("apptheme",
												"Black");
										editor.putBoolean("validation",
												true);

										editor.commit();
										Toast.makeText(
												Launcher.this,
												R.string.toastdefaultcolorsettings,
												Toast.LENGTH_SHORT).show();
										if (theme.equals("Light")) {
											reset = true;
										}
										onResume();
									}
								}).setNegativeButton("No", null).show();
			} catch (Exception e) {
				Toast.makeText(Launcher.this,
						R.string.toastdefaultcolorsettingserror,
						Toast.LENGTH_LONG).show();
			}
			return true;
			case R.id.aboutmenu:
			new AlertDialog.Builder(this)
					.setTitle(R.string.abouttitle)
					.setIcon(android.R.drawable.ic_menu_info_details)
					.setView(
							getLayoutInflater().inflate(R.layout.about,
									null)).show();
			return true;
		}
		return true;
	}
}