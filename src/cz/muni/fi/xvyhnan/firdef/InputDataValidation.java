package cz.muni.fi.xvyhnan.firdef;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.EditText;

/**
 * Controls inserted data when clicking on generate button.
 * @author Jan Vyhn�nek
 */
public class InputDataValidation {
	private final String ERROR = "!!!";
	private final int DEFAULT_TEXT_COLOR = Color.WHITE;
	private final int DEFAULT_TEXT_COLOR_WHITE = Color.BLACK;
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private SharedPreferences colorPreferences;

	public InputDataValidation(SharedPreferences colorPreferences) {
		this.colorPreferences = colorPreferences;
	}

	/**
	 * Determines whether the standard ACL number is correct.
	 * @param input Inserted standard ACL number.
	 * @return True if the standard ACL number is correct, false otherwise.
	 */
	public boolean isAclNumber(EditText input) {
		Pattern pattern = Pattern.compile("[0]*[1-9][0-9]?|[0]*[1][3-9][0-9]{2}");
        Matcher matcher = pattern.matcher(input.getText().toString());

        if(!matcher.matches()) {
        	setError(input, true);
            return false;
        } else {
            return true;
        }
	}

	/**
	 * Determines whether the extended ACL number is correct.
	 * @param input Inserted extended ACL number.
	 * @return True if the extended ACL number is correct, false otherwise.
	 */
	public boolean isExtendedAclNumber(EditText input) {
		Pattern pattern = Pattern.compile("[0]*[1][0-9]{2}|[0]*[2][0-6][0-9]{2}");
        Matcher matcher = pattern.matcher(input.getText().toString());

        if(!matcher.matches()) {
        	setError(input, true);
        	return false;
        } else {
            return true;
        }
	}

	/**
	 * Determines whether the IPv4 address is correct.
	 * @param inputs Inserted IPv4 octets.
	 * @return True if the IPv4 address is correct, false otherwise.
	 */
	public boolean isIpAddress(EditText[] inputs) {
		boolean ret = true;
		Pattern pattern = Pattern.compile("[0]*[2][5][0-5]|[0]*[2][0-4][0-9]|[0]*[1][0-9]{2}|[0]*[1-9][0-9]?|[0]*[0]");

		for(EditText input : inputs) {
			Matcher matcher = pattern.matcher(input.getText().toString());

	        if(!matcher.matches()) {
	        	setError(input, true);
	        	ret = false;
	        }
		}

		return ret;
	}

	/**
	 * Determines whether the IPv6 address is correct.
	 * @param inputs Inserted IPv6 parts.
	 * @return True if the IPv6 address is correct, false otherwise.
	 */
	public boolean isIPv6Address(EditText[] inputs) {
		boolean ret = true;
		Pattern pattern = Pattern.compile("[0]*[0-9a-f][0-9a-f]?[0-9a-f]?[0-9a-f]?");

		for(EditText input : inputs) {
			if(!input.getText().toString().equals("")) {
				Matcher matcher = pattern.matcher(input.getText().toString().toLowerCase());

				if(!matcher.matches()) {
					setError(input, true);
					ret = false;
				}
			}
		}

		return ret;
	}

	/**
	 * Determines whether the wildcard address is correct.
	 * @param inputs Inserted wildcard octets.
	 * @return True if the wildcard address is correct, false otherwise.
	 */
	public boolean isWildcard(EditText[] inputs) {
		return isIpAddress(inputs);
	}

	/**
	 * Determines whether the port range is correct.
	 * @param from Starting port number.
	 * @param to End port number.
	 * @return True if the port range is correct, false otherwise.
	 */
	public boolean isToFromCorrect(EditText from, EditText to) {
		if(!isAcceptedValue(from.getText().toString()) || !isAcceptedValue(to.getText().toString())) {
			if(!isAcceptedValue(from.getText().toString())) {
				setError(from, true);
			}
			if(!isAcceptedValue(to.getText().toString())) {
				setError(to, true);
			}
			return false;
		}

		int valueFrom = Integer.valueOf(from.getText().toString());
		int valueTo = Integer.valueOf(to.getText().toString());
		if(valueTo > valueFrom && valueTo >= 0 && valueTo <= 65535 && valueFrom >= 0 && valueFrom <= 65535) {
			return true;
		}

		if(valueFrom > valueTo) {
			setError(from, true);
		} else if(valueFrom > 65535){
			setError(from, true);
		} else if(valueTo > 65535) {
			setError(to, true);
		}
		return false;
	}

	/**
	 * Determines whether the port is correct.
	 * @param port Number of port.
	 * @return True if the port is correct, false otherwise.
	 */
	public boolean isPortCorrect(EditText port) {
		if(!isAcceptedValue(port.getText().toString())) {
			setError(port, true);
			return false;
		}

		int value = Integer.valueOf(port.getText().toString());
		if(value >= 0 && value <= 65535) {
			return true;
		}
		setError(port, true);
		return false;
	}

	/**
	 * Determines whether the mask of IPv4 is correct.
	 * @param port Number of mask.
	 * @return True if the mask is correct, false otherwise.
	 */
	public boolean isMaskCorrect(EditText mask) {
		if(!isAcceptedValue(mask.getText().toString())) {
			setError(mask, true);
			return false;
		}

		int value = Integer.valueOf(mask.getText().toString());
		if(value >= 0 && value <= 32) {
			return true;
		}
		setError(mask, true);
		return false;
	}

	/**
	 * Determines whether the mask of IPv6 is correct.
	 * @param port Number of mask.
	 * @return True if the mask is correct, false otherwise.
	 */
	public boolean isIPv6MaskCorrect(EditText mask) {
		if(!isAcceptedValue(mask.getText().toString())) {
			setError(mask, true);
			return false;
		}

		int value = Integer.valueOf(mask.getText().toString());
		if(value >= 0 && value <= 128) {
			return true;
		}
		setError(mask, true);
		return false;
	}

	/**
	 * Enables or disables error highlighting of control.
	 * @param control View which has an invalid value.
	 * @param enabled If true then method enables error highlighting, otherwise it disables it.
	 */
	private void setError(View control, boolean enabled) {
    	EditText et = (EditText)control;
    	if(enabled) {
    		et.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
    		et.setTextColor(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)));
    		et.setText(ERROR);
    	} else {
    		et.getBackground().clearColorFilter();
    		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
        	} else {
        		et.setTextColor(DEFAULT_TEXT_COLOR);
        	}
    		if(et.getText().toString().equals(ERROR)) {
    			et.setText("");
    		}
    	}
    }

	/**
	 * Determines whether the value in control does not contain the ERROR ("!!!") value or its substring.
	 * @param value Value in control.
	 * @return True if value is acceptable, false otherwise.
	 */
	private boolean isAcceptedValue(String value) {
		if(value.contains(ERROR) || value.contains(ERROR.substring(1)) || value.contains(ERROR.substring(2))) return false;
		return true;
	}
}
