package cz.muni.fi.xvyhnan.firdef;

import java.util.Set;

import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout.LayoutParams;

/**
 * Consists of the basic configuration methods.
 * @author Jan Vyhn�nek
 */
public interface IConfiguration {

	/**
	 * Creates IPv4 address.
	 * @param octets Array of IPv4 octets.
	 * @return IPv4 address.
	 */
	public String makeAddress(String[] octets);

	/**
	 * Creates IPv6 address.
	 * @param parts Array IPv6 parts.
	 * @return IPv6 address.
	 */
	public String makeIPv6Address(String[] parts);

	/**
	 * Removes zeros from the beginning of string.
	 * @param number String value of number or expression with numbers.
	 * @return Expression without zeros at the beginning.
	 */
	public String removeZeros(String number);

	/**
	 * Creates and shows alert dialog.
	 * @param title Title of alert dialog.
	 * @param alerts Set of alerts.
	 */
	public void makeAlertDialog(Set<String> alerts);

	/**
	 * Sets the enable and the focusable property.
	 * @param controls Array of views to be set up.
	 * @param bool Inserted value to enable and focusable property.
	 */
	public void setEnabledAndFocusable(View[] controls, boolean bool);

	/**
	 * Sets the enable and the focusable property.
	 * @param control View to be set up.
	 * @param bool Inserted value to enable and focusable property.
	 */
	public void setEnabledAndFocusable(View control, boolean bool);

	/**
	 * Sets the visibility property.
	 * @param control View to be set up.
	 * @param visibility Inserted value to visibility property.
	 */
	public void setVisibility(View control, int visibility);

	/**
	 * Sets the layout below property.
	 * @param control View to be set up.
	 * @param params New parameters which replace the old ones in control.
	 */
	public void setLayoutBelow(View control, LayoutParams params);

	/**
	 * Enables or disables error highlighting of control.
	 * @param control View which has an invalid value.
	 * @param enabled If true then method enables error highlighting, otherwise it disables it.
	 */
	public void setError(View control, boolean enabled);

	/**
	 * Determines whether the value in control is not the ERROR ("!!!") value.
	 * @param value Value in control.
	 * @return True if value is acceptable, false otherwise.
	 */
	public boolean isAcceptedValue(String value);

	/**
	 * Erases content of EditText.
	 * @param et EditText which content will be erased.
	 */
	public void eraseInsertedData(EditText et);

	/**
	 * Determines whether the IPv4 address is not fully filled in.
	 * @param octets Array of IPv4 octets.
	 * @return True if IPv4 is not fully filled in, false otherwise.
	 */
	public boolean isIPEmpty(EditText[] octets);

	/**
	 * Determines whether the IPv6 address is empty.
	 * @param octets Array of IPv6 parts.
	 * @return True if IPv6 is empty, false otherwise.
	 */
	public boolean isIPv6Empty(EditText[] octets);

	/**
     * Determines whether the IP address is fully empty. Does not produce error warning.
	 * @param octets Array of IP parts.
	 * @return True if IP is fully empty, false otherwise.
     */
    public boolean isIPEmptyWithoutErrorWarning(EditText[] octets);

	/**
     * Changes the shown IP version.
     * @param v Pressed radio button.
     */
	public void onIPSelectionClick(View v);

	/**
     * Enables to insert the other protocol.
     * @param v Pressed check box.
     */
    public void onOtherProtocolClick(View v);

	/**
     * Initializes all attributes.
     */
    public void initialization();
}
