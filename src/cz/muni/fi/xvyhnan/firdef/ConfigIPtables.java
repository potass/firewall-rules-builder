package cz.muni.fi.xvyhnan.firdef;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.components.MultiSpinner;
import cz.muni.fi.xvyhnan.firdef.validation.IPv4MaskValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv4Validation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6MaskValidation;
import cz.muni.fi.xvyhnan.firdef.validation.IPv6Validation;
import cz.muni.fi.xvyhnan.firdef.validation.PortValidation;

/**
 * Shows configuration window of IPtables.
 * @author Jan Vyhn�nek
 */
public class ConfigIPtables extends SherlockActivity implements IConfiguration {
	private final String ERROR = "!!!";
	private final int DEFAULT_TEXT_COLOR = Color.WHITE;
	private final int DEFAULT_TEXT_COLOR_WHITE = Color.BLACK;
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private final boolean DEFAULT_VALIDATION = true;
	private InputDataValidation validator;
	private IPv6Validation ipv6Validation;
	private int selectedCommand = 0, selectedTable = 0, selectedTarget = 0;
	private Spinner commands, tables, targets, protocols;
	private TextView table, otherUp, all, inInterface, outInterface, sourceIP, destinationIP,
			target, otherDown, state, point1, point2, point3, point4, point5, point6,
			doublePoint1, doublePoint2, doublePoint3, doublePoint4, doublePoint5, doublePoint6, doublePoint7,
			doublePoint8, doublePoint9, doublePoint10, doublePoint11, doublePoint12, doublePoint13, doublePoint14,
			otherProtocol;
	private View[] allControlsForDisable, newChainControls, policyControls;
	private EditText[] sourceIPv4Controls, destinationIPv4Controls, sourceIPv6Controls,
			destinationIPv6Controls;
	private EditText otherTableET, otherProtocolET, inInterfaceET, outInterfaceET, sourceIPET1, sourceIPET2,
			sourceIPET3, sourceIPET4, destinationIPET1, destinationIPET2, destinationIPET3, destinationIPET4,
			sourceMaskET, sourcePortET, destinationMaskET, destinationPortET, otherTargetET, sourceIPv6ET1,
			sourceIPv6ET2, sourceIPv6ET3, sourceIPv6ET4, sourceIPv6ET5, sourceIPv6ET6, sourceIPv6ET7,
			sourceIPv6ET8, destinationIPv6ET1, destinationIPv6ET2, destinationIPv6ET3, destinationIPv6ET4,
			destinationIPv6ET5, destinationIPv6ET6, destinationIPv6ET7, destinationIPv6ET8;
	private CheckBox otherTableChB, allChB, otherTargetChB, otherProtocolChB;
	private MultiSpinner states;
	private List<String> statesItems;
	private RadioGroup ipv4Ipv6;
	private RadioButton ipv4, ipv6;
	private RelativeLayout sourceIPv4RL, destinationIPv4RL, sourceIPv6RL, destinationIPv6RL, destinationIPHeader,
			sourceMaskPortHeader;
	private LayoutParams destinationIPP, sourceMaskPortP;
	private TextWatcher watcher1, watcher2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

        super.onCreate(savedInstanceState);
        setContentView(R.layout.configiptables);
        setTitle(getString(R.string.activityiptablestitle));

        addListenerOnCommandsItemSelection();
        addListenerOnTablesItemSelection();
        addListenerOnTargetsItemSelection();
        addListenerOnMultiSpinner();

        initialization();
    }

    @Override
    public void onResume() {
    	super.onResume();

    	for(EditText e : sourceIPv4Controls) {
			setError(e, false);
		}
		for(EditText e : destinationIPv4Controls) {
			setError(e, false);
		}
		for(EditText e : sourceIPv6Controls) {
			setError(e, false);
		}
		for(EditText e : destinationIPv6Controls) {
			setError(e, false);
		}

		setError(otherTableET, false);
		setError(otherProtocolET, false);
		setError(otherTargetET, false);
		setError(sourceMaskET, false);
		setError(destinationMaskET, false);
		setError(sourcePortET, false);
		setError(destinationPortET, false);
		setError(targets, false);
    }

    /**
     * Adds listener on the command selection spinner.
     */
    private void addListenerOnCommandsItemSelection() {
    	commands = (Spinner) findViewById(R.id.typeofcommand);
    	commands.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()	{
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 0) {
    	    		otherTableChB.setChecked(false);
    	    		otherTargetChB.setChecked(false);
    	    		allChB.setChecked(false);
    	    		otherProtocolChB.setChecked(false);
    	    		setEnabledAndFocusable(allControlsForDisable, true);
    	    		setEnabledAndFocusable(newChainControls, false);
    	    		setEnabledAndFocusable(otherTargetET, false);
    	    		setEnabledAndFocusable(otherProtocolET, false);
    	    		selectedCommand = 0;
    	    	} else if(position == 1) {
    	    		otherTableChB.setChecked(false);
    	    		otherTargetChB.setChecked(false);
    	    		allChB.setChecked(false);
    	    		otherProtocolChB.setChecked(false);
    	    		setEnabledAndFocusable(allControlsForDisable, true);
    	    		setEnabledAndFocusable(newChainControls, false);
    	    		setEnabledAndFocusable(otherTargetET, false);
    	    		setEnabledAndFocusable(otherProtocolET, false);
    	    		selectedCommand = 1;
    	    	} else if(position == 2) {
    	    		otherTableChB.setChecked(true);
    	    		otherTargetChB.setChecked(false);
    	    		allChB.setChecked(false);
    	    		otherProtocolChB.setChecked(false);
    	    		setEnabledAndFocusable(allControlsForDisable, false);
    	    		setEnabledAndFocusable(newChainControls, true);
    	    		setEnabledAndFocusable(otherTargetET, false);
    	    		setEnabledAndFocusable(otherProtocolET, false);
    	    		selectedCommand = 2;
    	    	} else {
    	    		otherTableChB.setChecked(false);
    	    		otherTargetChB.setChecked(false);
    	    		allChB.setChecked(false);
    	    		otherProtocolChB.setChecked(false);
    	    		setEnabledAndFocusable(allControlsForDisable, false);
    	    		setEnabledAndFocusable(newChainControls, false);
    	    		setEnabledAndFocusable(policyControls, true);
    	    		setEnabledAndFocusable(otherTargetET, false);
    	    		setEnabledAndFocusable(otherProtocolET, false);
    	    		selectedCommand = 3;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	otherTableChB.setChecked(false);
	    		otherTargetChB.setChecked(false);
	    		allChB.setChecked(false);
	    		setEnabledAndFocusable(allControlsForDisable, true);
	    		setEnabledAndFocusable(newChainControls, false);
	    		setEnabledAndFocusable(otherTargetET, false);
	    		selectedCommand = 0;
    	    }
    	});
    }

    /**
     * Adds listener on the table selection spinner.
     */
    private void addListenerOnTablesItemSelection() {
    	tables = (Spinner) findViewById(R.id.typeoftable);
    	tables.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 0) {
    	    		selectedTable = 0;
    	    	} else if(position == 1) {
    	    		selectedTable = 1;
    	    	} else {
    	    		selectedTable = 2;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedTable = 0;
    	    }
    	});
    }

    /**
     * Adds listener on the target selection spinner.
     */
    private void addListenerOnTargetsItemSelection() {
    	targets = (Spinner) findViewById(R.id.typeoftarget);
    	targets.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
    	    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    	    	if(position == 1) {
    	    		selectedTarget = 1;
    	    	} else if(position == 2) {
    	    		selectedTarget = 2;
    	    	} else if(position == 3) {
    	    		selectedTarget = 3;
    	    	} else {
    	    		selectedTarget = 0;
    	    	}
    	    }

    	    public void onNothingSelected(AdapterView<?> arg0) {
    	    	selectedTarget = 0;
    	    }
    	});
    }

    /**
     * Adds listener on the state selection spinner.
     */
    private void addListenerOnMultiSpinner() {
    	states = (MultiSpinner) findViewById(R.id.iptstateMS);

    	statesItems = new ArrayList<String>();
    	statesItems.add("NEW");
    	statesItems.add("ESTABLISHED");
    	statesItems.add("RELATED");
    	statesItems.add("INVALID");

    	states.setItems(statesItems, getString(R.string.sstate), new MultiSpinner.MultiSpinnerListener() {
    		public void onItemsSelected(boolean[] selected) {}
    	});
    }

    public void onIPSelectionClick(View v) {
    	int checkedIP = ipv4Ipv6.getCheckedRadioButtonId();
    	if(checkedIP == R.id.iptipv4) {
    		destinationIPP.addRule(RelativeLayout.BELOW, R.id.iptsourceipRL);
    		sourceMaskPortP.addRule(RelativeLayout.BELOW, R.id.iptdestinationipRL);

    		setLayoutBelow(destinationIPHeader, destinationIPP);
    		setLayoutBelow(sourceMaskPortHeader, sourceMaskPortP);

    		sourceMaskET.setText("");
    		sourceMaskET.setHint(R.string.hmask);
    		sourceMaskET.removeTextChangedListener(watcher2);
    		sourceMaskET.addTextChangedListener(watcher1);
    		destinationMaskET.setText("");
    		destinationMaskET.setHint(R.string.hmask);
    		destinationMaskET.removeTextChangedListener(watcher2);
    		destinationMaskET.addTextChangedListener(watcher1);

			setVisibility(sourceIPv4RL, 0);
			setVisibility(destinationIPv4RL, 0);
			setVisibility(sourceIPv6RL, 4);
			setVisibility(destinationIPv6RL, 4);
    	} else {
    		destinationIPP.addRule(RelativeLayout.BELOW, R.id.iptsourceipv6RL);
    		sourceMaskPortP.addRule(RelativeLayout.BELOW, R.id.iptdestinationipv6RL);

    		setLayoutBelow(destinationIPHeader, destinationIPP);
    		setLayoutBelow(sourceMaskPortHeader, sourceMaskPortP);

    		sourceMaskET.setText("");
    		sourceMaskET.setHint(R.string.hmask2);
    		sourceMaskET.removeTextChangedListener(watcher1);
    		sourceMaskET.addTextChangedListener(watcher2);
    		destinationMaskET.setText("");
    		destinationMaskET.setHint(R.string.hmask2);
    		destinationMaskET.removeTextChangedListener(watcher1);
    		destinationMaskET.addTextChangedListener(watcher2);

    		setVisibility(sourceIPv4RL, 4);
    		setVisibility(destinationIPv4RL, 4);
    		setVisibility(sourceIPv6RL, 0);
			setVisibility(destinationIPv6RL, 0);
    	}
    }

    /**
     * Enable to insert the other table parameter.
     * @param v Pressed check box.
     */
    public void onOtherTableClick(View v) {
    	if(otherTableChB.isChecked()) {
    		setEnabledAndFocusable(otherTableET, true);
    		setEnabledAndFocusable(table, false);
    		setEnabledAndFocusable(tables, false);
    	} else {
    		setEnabledAndFocusable(otherTableET, false);
    		setEnabledAndFocusable(table, true);
    		setEnabledAndFocusable(tables, true);
    	}
    }

    /**
     * Enable to insert the other target parameter.
     * @param v Pressed check box.
     */
    public void onOtherTargetClick(View v) {
    	if(otherTargetChB.isChecked()) {
    		setEnabledAndFocusable(otherTargetET, true);
    		setEnabledAndFocusable(target, false);
    		setEnabledAndFocusable(targets, false);
    	} else {
    		setEnabledAndFocusable(otherTargetET, false);
    		setEnabledAndFocusable(target, true);
    		setEnabledAndFocusable(targets, true);
    	}
    }

    /**
     * Enable to accept all existing protocols.
     * @param v Pressed check box.
     */
    public void onAllClick(View v) {
    	if(allChB.isChecked()) {
    		setEnabledAndFocusable(otherProtocolET, false);
    		setEnabledAndFocusable(otherProtocolChB, false);
    		setEnabledAndFocusable(otherProtocol, false);
    		setEnabledAndFocusable(protocols, false);
    	} else {
    		setEnabledAndFocusable(otherProtocolChB, true);
    		setEnabledAndFocusable(otherProtocol, true);
    		if(otherProtocolChB.isChecked()) {
    			setEnabledAndFocusable(otherProtocolET, true);
    		} else {
    			setEnabledAndFocusable(protocols, true);
    		}
    	}
    }

    public void onOtherProtocolClick(View v) {
    	if(otherProtocolChB.isChecked()) {
    		setEnabledAndFocusable(otherProtocolET, true);
    		setEnabledAndFocusable(protocols, false);
    	} else {
    		setEnabledAndFocusable(otherProtocolET, false);
    		setEnabledAndFocusable(protocols, true);
    	}
    }

    /**
     * Validates inserted values, generates the filtering rule and shows it.
     * @param v Pressed button.
     */
    public void onGenerateClick(View v) {
    	boolean error = false;

    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);

    	Set<String> alert = new HashSet<String>();

    	Set<EditText> errorControls = new HashSet<EditText>();

    	String[] sourceIPAddress = new String[]{sourceIPET1.getText().toString(),
				sourceIPET2.getText().toString(), sourceIPET3.getText().toString(),
				sourceIPET4.getText().toString()};
		String[] destinationIPAddress = new String[]{destinationIPET1.getText().toString(),
				destinationIPET2.getText().toString(), destinationIPET3.getText().toString(),
    			destinationIPET4.getText().toString()};
		String[] sourceIPv6Address = new String[]{sourceIPv6ET1.getText().toString(),
				sourceIPv6ET2.getText().toString(), sourceIPv6ET3.getText().toString(),
				sourceIPv6ET4.getText().toString(), sourceIPv6ET5.getText().toString(),
				sourceIPv6ET6.getText().toString(), sourceIPv6ET7.getText().toString(),
				sourceIPv6ET8.getText().toString()};
		String[] destinationIPv6Address = new String[]{destinationIPv6ET1.getText().toString(),
				destinationIPv6ET2.getText().toString(), destinationIPv6ET3.getText().toString(),
				destinationIPv6ET4.getText().toString(), destinationIPv6ET5.getText().toString(),
				destinationIPv6ET6.getText().toString(), destinationIPv6ET7.getText().toString(),
				destinationIPv6ET8.getText().toString()};

		if(colorPreferences.getBoolean("validation", DEFAULT_VALIDATION)) {

			ipv6Validation.setError(true);

			for (EditText e : sourceIPv4Controls) {
				setError(e, false);
			}
			for (EditText e : destinationIPv4Controls) {
				setError(e, false);
			}
			for (EditText e : sourceIPv6Controls) {
				setError(e, false);
			}
			for (EditText e : destinationIPv6Controls) {
				setError(e, false);
			}

			setError(otherTableET, false);
			setError(otherProtocolET, false);
			setError(otherTargetET, false);
			setError(sourceMaskET, false);
			setError(destinationMaskET, false);
			setError(sourcePortET, false);
			setError(destinationPortET, false);
			setError(targets, false);

			if (selectedCommand == 0 || selectedCommand == 1) {
				if (otherTableChB.isChecked()
						&& (otherTableET.getText().toString()
								.replaceAll(" ", "").equals("") || !isAcceptedValue(otherTableET
								.getText().toString()))) {
					alert.add(getString(R.string.missingothertable) + "\n\n");
					setError(otherTableET, true);
					errorControls.add(otherTableET);
					error = true;
				}

				if (!allChB.isChecked()
						&& otherProtocolChB.isChecked()
						&& (otherProtocolET.getText().toString()
								.replaceAll(" ", "").equals("") || !isAcceptedValue(otherProtocolET
								.getText().toString()))) {
					alert.add(getString(R.string.missingotherprotocol) + "\n\n");
					setError(otherProtocolET, true);
					errorControls.add(otherProtocolET);
					error = true;
				}

				if (otherTargetChB.isChecked()
						&& (otherTargetET.getText().toString()
								.replaceAll(" ", "").equals("") || !isAcceptedValue(otherTargetET
								.getText().toString()))) {
					alert.add(getString(R.string.missingothertarget) + "\n\n");
					setError(otherTargetET, true);
					errorControls.add(otherTargetET);
					error = true;
				}

				if (ipv4Ipv6.getCheckedRadioButtonId() == R.id.iptipv4) {
					if (!isIPEmptyWithoutErrorWarning(sourceIPv4Controls)
							&& !validator.isIpAddress(sourceIPv4Controls)) {
						alert.add(getString(R.string.wrongsourceip) + "\n\n");
						error = true;
					}

					if (!isIPEmptyWithoutErrorWarning(destinationIPv4Controls)
							&& !validator.isIpAddress(destinationIPv4Controls)) {
						alert.add(getString(R.string.wrongsourceip) + "\n\n");
						error = true;
					}

					if (!sourceMaskET.getText().toString().replaceAll(" ", "")
							.equals("")
							&& !validator.isMaskCorrect(sourceMaskET)) {
						alert.add(getString(R.string.wrongmask) + "\n\n");
						error = true;
					}

					if (!sourceMaskET.getText().toString().replaceAll(" ", "")
							.equals("")
							&& isIPEmpty(sourceIPv4Controls)) {
						alert.add(getString(R.string.missingip) + "\n\n");
						error = true;
					}

					if (!destinationMaskET.getText().toString()
							.replaceAll(" ", "").equals("")
							&& !validator.isMaskCorrect(destinationMaskET)) {
						alert.add(getString(R.string.wrongmask) + "\n\n");
						error = true;
					}

					if (!destinationMaskET.getText().toString()
							.replaceAll(" ", "").equals("")
							&& isIPEmpty(destinationIPv4Controls)) {
						alert.add(getString(R.string.missingip) + "\n\n");
						error = true;
					}
				} else {
					if (!validator.isIPv6Address(sourceIPv6Controls)) {
						alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
						error = true;
					}

					if (!validator.isIPv6Address(destinationIPv6Controls)) {
						alert.add(getString(R.string.wrongsourceipv6) + "\n\n");
						error = true;
					}

					if (!sourceMaskET.getText().toString().replaceAll(" ", "")
							.equals("")
							&& !validator.isIPv6MaskCorrect(sourceMaskET)) {
						alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
						error = true;
					}

					if (!sourceMaskET.getText().toString().replaceAll(" ", "")
							.equals("")
							&& isIPv6Empty(sourceIPv6Controls)) {
						alert.add(getString(R.string.missingip) + "\n\n");
						error = true;
					}

					if (!destinationMaskET.getText().toString()
							.replaceAll(" ", "").equals("")
							&& !validator.isIPv6MaskCorrect(destinationMaskET)) {
						alert.add(getString(R.string.wrongmaskipv6) + "\n\n");
						error = true;
					}

					if (!destinationMaskET.getText().toString()
							.replaceAll(" ", "").equals("")
							&& isIPv6Empty(destinationIPv6Controls)) {
						alert.add(getString(R.string.missingip) + "\n\n");
						error = true;
					}
				}

				if (!sourcePortET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isPortCorrect(sourcePortET)) {
					alert.add(getString(R.string.wrongport) + "\n\n");
					error = true;
				}

				if (!destinationPortET.getText().toString().replaceAll(" ", "")
						.equals("")
						&& !validator.isPortCorrect(destinationPortET)) {
					alert.add(getString(R.string.wrongport) + "\n\n");
					error = true;
				}

			} else if (selectedCommand == 2) {
				if (otherTableET.getText().toString().replaceAll(" ", "")
						.equals("")
						|| !isAcceptedValue(otherTableET.getText().toString())) {
					alert.add(getString(R.string.missingothertable) + "\n\n");
					setError(otherTableET, true);
					errorControls.add(otherTableET);
					error = true;
				}
			} else {
				if (selectedTarget == 0) {
					setError(targets, true);
					alert.add(getString(R.string.missingnone) + "\n\n");
					error = true;
				}
			}

			ipv6Validation.setError(false);
		}

    	if(error) {
    		makeAlertDialog(alert);
    	} else {
    		String command = "";
    		if(ipv4Ipv6.getCheckedRadioButtonId() == R.id.iptipv4) {
    			command = "iptables";
    		} else {
    			command = "ip6tables";
    		}

    		if(selectedCommand == 0 || selectedCommand == 1) {
        		command += (selectedCommand == 0 ? " -A" : " -D");

        		if(otherTableChB.isChecked()) {
        			command += " "+ otherTableET.getText().toString();
        		} else {
        			if(selectedTable == 0) {
            			command += " INPUT";
            		} else if(selectedTable == 1) {
            			command += " OUTPUT";
            		} else {
            			command += " FORWARD";
            		}
        		}

        		if(!allChB.isChecked() && !otherProtocolChB.isChecked()) {
        			String selectedProtocol = protocols.getSelectedItem().toString();
        			if(!selectedProtocol.equals("none")) {
        				command += " -p "+ selectedProtocol;
        			}
        		} else if(allChB.isChecked()) {
        			command += " -p ALL";
        		} else if(!allChB.isChecked() && otherProtocolChB.isChecked()) {
        			command += " -p "+ otherProtocolET.getText().toString();
        		}

        		if(!inInterfaceET.getText().toString().replaceAll(" ", "").equals("")) {
        			command += " -i "+ inInterfaceET.getText().toString();
        		}

        		if(!outInterfaceET.getText().toString().replaceAll(" ", "").equals("")) {
        			command += " -o "+ outInterfaceET.getText().toString();
        		}

        		if(ipv4Ipv6.getCheckedRadioButtonId() == R.id.iptipv4) {
        			if(!isIPEmptyWithoutErrorWarning(sourceIPv4Controls)) {
        				command += " -s "+ makeAddress(sourceIPAddress);
        				if(!sourceMaskET.getText().toString().replaceAll(" ", "").equals("")) {
        					command += "/"+ removeZeros(sourceMaskET.getText().toString());
        				}
        			}
        		} else {
        			if(!isIPEmptyWithoutErrorWarning(sourceIPv6Controls)) {
        				command += " -s "+ makeIPv6Address(sourceIPv6Address);
        				if(!sourceMaskET.getText().toString().replaceAll(" ", "").equals("")) {
        					command += "/"+ removeZeros(sourceMaskET.getText().toString());
        				}
        			}
        		}

        		if(!sourcePortET.getText().toString().replaceAll(" ", "").equals("")) {
    				command += " --sport "+ removeZeros(sourcePortET.getText().toString());
    			}

        		if(ipv4Ipv6.getCheckedRadioButtonId() == R.id.iptipv4) {
        			if(!isIPEmptyWithoutErrorWarning(destinationIPv4Controls)) {
        				command += " -d "+ makeAddress(destinationIPAddress);
        				if(!destinationMaskET.getText().toString().replaceAll(" ", "").equals("")) {
        					command += "/"+ removeZeros(destinationMaskET.getText().toString());
        				}
        			}
        		} else {
        			if(!isIPEmptyWithoutErrorWarning(destinationIPv6Controls)) {
        				command += " -d "+ makeIPv6Address(destinationIPv6Address);
        				if(!destinationMaskET.getText().toString().replaceAll(" ", "").equals("")) {
        					command += "/"+ removeZeros(destinationMaskET.getText().toString());
        				}
        			}
        		}

        		if(!destinationPortET.getText().toString().replaceAll(" ", "").equals("")) {
    				command += " --dport "+ removeZeros(destinationPortET.getText().toString());
    			}

        		if(!states.getSelectedItems().replaceAll(" ", "").equals("")) {
        			command += " -m state --state "+ states.getSelectedItems();
        		}

        		if(otherTargetChB.isChecked() && !otherTargetET.getText().toString().replaceAll(" ", "").equals("")) {
        			command += " -j "+ otherTargetET.getText().toString();
        		} else {
        			if(selectedTarget == 1) {
            			command += " -j ACCEPT";
            		} else if(selectedTarget == 2) {
            			command += " -j DROP";
            		} else if(selectedTarget == 3) {
            			command += " -j LOG";
            		}
        		}
        	} else if(selectedCommand == 2) {
        		command += " -N";
        		command += " "+ otherTableET.getText().toString();
        	} else {
        		command += " -P";

        		if(selectedTable == 0) {
        			command += " INPUT";
        		} else if(selectedTable == 1) {
        			command += " OUTPUT";
        		} else {
        			command += " FORWARD";
        		}

        		if(selectedTarget == 1) {
        			command += " ACCEPT";
        		} else if(selectedTarget == 2) {
        			command += " DROP";
        		} else if(selectedTarget == 3) {
        			command += " LOG";
        		}
        	}

    		Intent intent = new Intent(this, ShowCommand.class);
    		intent.putExtra("command", command);
    		startActivity(intent);
    	}
    }

    /**
     * Erases the content of specific EditText(s).
     * @param v Pressed button.
     */
    public void onEraseClick(View v) {
    	switch (v.getId()) {
			case R.id.ipttableerase:
				eraseInsertedData(otherTableET);
				break;
			case R.id.iptprotocolerase:
				eraseInsertedData(otherProtocolET);
				break;
			case R.id.iptininterfaceerase:
				eraseInsertedData(inInterfaceET);
				break;
			case R.id.iptoutinterfaceerase:
				eraseInsertedData(outInterfaceET);
				break;
			case R.id.iptsourceiperase:
				if(ipv4.isChecked()) {
					for (EditText et : sourceIPv4Controls) {
						eraseInsertedData(et);
					}
				} else {
					for (EditText et : sourceIPv6Controls) {
						eraseInsertedData(et);
					}
				}
				break;
			case R.id.iptdestinationiperase:
				if(ipv4.isChecked()) {
					for (EditText et : destinationIPv4Controls) {
						eraseInsertedData(et);
					}
				} else {
					for (EditText et : destinationIPv6Controls) {
						eraseInsertedData(et);
					}
				}
				break;
			case R.id.iptsourcemaskporterase:
				eraseInsertedData(sourceMaskET);
				eraseInsertedData(sourcePortET);
				break;
			case R.id.iptdestinationmaskporterase:
				eraseInsertedData(destinationMaskET);
				eraseInsertedData(destinationPortET);
				break;
			case R.id.ipttargeterase:
				eraseInsertedData(otherTargetET);
				break;
    	}
    }

    public void eraseInsertedData(EditText et) {
    	SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
    	} else {
    		et.setTextColor(DEFAULT_TEXT_COLOR);
    	}
		et.setText("");
    }

    public void setEnabledAndFocusable(View[] controls, boolean bool) {
    	for(View control : controls) {
    		setEnabledAndFocusable(control, bool);
		}
    }

    public void setEnabledAndFocusable(View control, boolean bool) {
    	control.setEnabled(bool);
    	if(!(control instanceof CheckBox) && !(control instanceof Spinner) && !(control instanceof RadioButton)) {
    		control.setFocusable(bool);
			control.setFocusableInTouchMode(bool);
    	}
    }

    public String makeAddress(String[] octets) {
    	String ret = "";
    	for(int i = 0; i < octets.length; i++) {
    		while(octets[i].length() > 1 && octets[i].startsWith("0")) {
    			octets[i] = octets[i].substring(1);
    		}
    		ret += octets[i];
    		if(i != octets.length - 1) {
    			ret += ".";
    		}
    	}
    	return ret;
    }

    public String makeIPv6Address(String[] parts) {
    	for(int i = 0; i < parts.length; i++) {
    		if(parts[i].length() > 0) {
    			while(parts[i].length() > 1 && parts[i].startsWith("0")) {
    				parts[i] = parts[i].substring(1);
    			}
    		} else {
    			parts[i] = "0";
    		}
    	}

    	int begin = -1, end = -1;
    	for(int i = 0; i < parts.length - 1; i++) {
    		if(parts[i].equals("0") && parts[i + 1].equals("0")) {
    			if(begin == -1) {
    				begin = i;
    			}
    			end = i + 1;
    		} else if(i > 0 && parts[i - 1].equals("0") && parts[i].equals("0") && !parts[i + 1].equals("0")) {
    			break;
    		}
    	}

    	String ret = "";
    	for(int i = 0; i < parts.length; i++) {
    		if(i == begin || i == end) {
    			ret += ":";
    		} else if(i < begin || i > end) {
    			ret += parts[i].toUpperCase();
        		if(i != parts.length - 1) {
        			ret += ":";
        		}
    		}
    	}
    	ret = ret.replace(":::", "::");

    	return ret;
    }

    public String removeZeros(String number) {
    	while(number.length() > 1 && number.startsWith("0")) {
    		number = number.substring(1);
		}
    	return number;
    }

    public void makeAlertDialog(Set<String> alerts) {
    	String alert = "";
    	for(String s : alerts) {
    		alert += s;
    	}

    	ScrollView alertScroll = new ScrollView(getApplicationContext());
    	TextView alertText = new TextView(getApplicationContext());
    	alertText.setText(alert.subSequence(0, alert.length() - 2));
    	alertText.setTypeface(null, Typeface.BOLD);
    	alertText.setTextColor(Color.rgb(255, 172, 0));
    	alertText.setTextSize(16);
    	alertText.setGravity(17);
    	alertText.setPadding(4, 0, 4, 0);
    	alertScroll.addView(alertText);

    	AlertDialog.Builder dialog = new AlertDialog.Builder(this);
    	dialog.setIcon(android.R.drawable.ic_dialog_alert);
    	dialog.setTitle("Errors of inserted values");
    	dialog.setView(alertScroll);
    	dialog.setPositiveButton("OK", null);
    	dialog.show();
    }

    public boolean isIPEmpty(EditText[] octets) {
    	boolean ret = false;
    	for(EditText octet : octets) {
    		if(octet.getText().toString().replaceAll(" ", "").equals("")) {
				setError(octet, true);
				ret = true;
    		}
    	}
    	return ret;
    }

    public boolean isIPv6Empty(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	if(ret) {
    		for(EditText octet : octets) {
        		setError(octet, true);
    		}
    	}
    	return ret;
    }

    public boolean isIPEmptyWithoutErrorWarning(EditText[] octets) {
    	boolean ret = true;
    	for(EditText octet : octets) {
    		if(!octet.getText().toString().replaceAll(" ", "").equals("")) {
    			ret = false;
    		}
    	}
    	return ret;
    }

    public void setVisibility(View control, int visibility) {
		control.setVisibility(visibility);
    }

    public void setLayoutBelow(View control, LayoutParams params) {
		control.setLayoutParams(params);
    }

    public void setError(View control, boolean enabled) {
    	if(control instanceof Spinner) {
    		Spinner s = (Spinner)control;
    		if(enabled) {
        		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        		s.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
        	} else {
        		s.getBackground().clearColorFilter();
        	}
    	} else {
    		EditText et = (EditText)control;
    		if(enabled) {
        		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        		et.getBackground().setColorFilter(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
        		et.setTextColor(Integer.valueOf(colorPreferences.getString("errorscolor", DEFAULT_ERROR_COLOR)));
        		et.setText(ERROR);
        	} else {
        		et.getBackground().clearColorFilter();
        		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
        			et.setTextColor(DEFAULT_TEXT_COLOR_WHITE);
            	} else {
            		et.setTextColor(DEFAULT_TEXT_COLOR);
            	}
        		if(et.getText().toString().equals(ERROR)) {
        			et.setText("");
        		}
        	}
    	}
    }

    public boolean isAcceptedValue(String value) {
		if(value.equals(ERROR)) return false;
		return true;
	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	MenuItem help = menu.add("Help");
    	help.setIcon(android.R.drawable.ic_menu_help);
    	help.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    	help.setIntent(new Intent(this, Help.class));
		return true;
	}

    public void initialization() {
    	validator = new InputDataValidation(PreferenceManager.getDefaultSharedPreferences(this));

    	watcher1 = new IPv4MaskValidation();
    	watcher2 = new IPv6MaskValidation();

    	protocols = (Spinner) findViewById(R.id.ipttypeofprotocol);
    	table = (TextView) findViewById(R.id.ipttable);
    	otherUp = (TextView) findViewById(R.id.iptotherup);
    	all = (TextView) findViewById(R.id.iptall);
    	inInterface = (TextView) findViewById(R.id.iptininterface);
    	outInterface = (TextView) findViewById(R.id.iptoutinterface);
    	sourceIP = (TextView) findViewById(R.id.iptsourceip);
    	destinationIP = (TextView) findViewById(R.id.iptdestinationip);
    	target = (TextView) findViewById(R.id.ipttarget);
    	otherDown = (TextView) findViewById(R.id.iptotherdown);
    	state = (TextView) findViewById(R.id.iptstate);
    	point1 = (TextView) findViewById(R.id.iptpoint1);
    	point2 = (TextView) findViewById(R.id.iptpoint1);
    	point3 = (TextView) findViewById(R.id.iptpoint1);
    	point4 = (TextView) findViewById(R.id.iptpoint1);
    	point5 = (TextView) findViewById(R.id.iptpoint1);
    	point6 = (TextView) findViewById(R.id.iptpoint1);
    	doublePoint1 = (TextView) findViewById(R.id.iptdoublepoint1);
    	doublePoint2 = (TextView) findViewById(R.id.iptdoublepoint2);
    	doublePoint3 = (TextView) findViewById(R.id.iptdoublepoint3);
    	doublePoint4 = (TextView) findViewById(R.id.iptdoublepoint4);
    	doublePoint5 = (TextView) findViewById(R.id.iptdoublepoint5);
    	doublePoint6 = (TextView) findViewById(R.id.iptdoublepoint6);
    	doublePoint7 = (TextView) findViewById(R.id.iptdoublepoint7);
    	doublePoint8 = (TextView) findViewById(R.id.iptdoublepoint8);
    	doublePoint9 = (TextView) findViewById(R.id.iptdoublepoint9);
    	doublePoint10 = (TextView) findViewById(R.id.iptdoublepoint10);
    	doublePoint11 = (TextView) findViewById(R.id.iptdoublepoint11);
    	doublePoint12 = (TextView) findViewById(R.id.iptdoublepoint12);
    	doublePoint13 = (TextView) findViewById(R.id.iptdoublepoint13);
    	doublePoint14 = (TextView) findViewById(R.id.iptdoublepoint14);
    	otherProtocol = (TextView) findViewById(R.id.iptotherprotocol);
    	otherTableChB = (CheckBox) findViewById(R.id.iptothertableChB);
    	allChB = (CheckBox) findViewById(R.id.iptallChB);
    	otherTargetChB = (CheckBox) findViewById(R.id.iptothertargetChB);
    	otherProtocolChB = (CheckBox) findViewById(R.id.iptotherprotocolChB);
    	otherTableET = (EditText) findViewById(R.id.iptothertableET);
    	otherProtocolET = (EditText) findViewById(R.id.iptotherprotocolET);
    	inInterfaceET = (EditText) findViewById(R.id.iptininterfaceET);
    	outInterfaceET = (EditText) findViewById(R.id.iptoutinterfaceET);
    	sourceIPET1 = (EditText) findViewById(R.id.iptsourceipET1);
    	sourceIPET1.addTextChangedListener(new IPv4Validation());
    	sourceIPET2 = (EditText) findViewById(R.id.iptsourceipET2);
    	sourceIPET2.addTextChangedListener(new IPv4Validation());
    	sourceIPET3 = (EditText) findViewById(R.id.iptsourceipET3);
    	sourceIPET3.addTextChangedListener(new IPv4Validation());
    	sourceIPET4 = (EditText) findViewById(R.id.iptsourceipET4);
    	sourceIPET4.addTextChangedListener(new IPv4Validation());
    	destinationIPET1 = (EditText) findViewById(R.id.iptdestinationipET1);
    	destinationIPET1.addTextChangedListener(new IPv4Validation());
    	destinationIPET2 = (EditText) findViewById(R.id.iptdestinationipET2);
    	destinationIPET2.addTextChangedListener(new IPv4Validation());
    	destinationIPET3 = (EditText) findViewById(R.id.iptdestinationipET3);
    	destinationIPET3.addTextChangedListener(new IPv4Validation());
    	destinationIPET4 = (EditText) findViewById(R.id.iptdestinationipET4);
    	destinationIPET4.addTextChangedListener(new IPv4Validation());
    	sourceMaskET = (EditText) findViewById(R.id.iptsourcemaskET);
    	sourceMaskET.addTextChangedListener(watcher1);
    	sourcePortET = (EditText) findViewById(R.id.iptsourceportET);
    	sourcePortET.addTextChangedListener(new PortValidation());
    	destinationMaskET = (EditText) findViewById(R.id.iptdestinationmaskET);
    	destinationMaskET.addTextChangedListener(watcher1);
    	destinationPortET = (EditText) findViewById(R.id.iptdestinationportET);
    	destinationPortET.addTextChangedListener(new PortValidation());
    	otherTargetET = (EditText) findViewById(R.id.iptothertargetET);

    	ipv6Validation = new IPv6Validation();

    	sourceIPv6ET1 = (EditText) findViewById(R.id.iptsourceipv6ET1);
    	sourceIPv6ET1.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET2 = (EditText) findViewById(R.id.iptsourceipv6ET2);
    	sourceIPv6ET2.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET3 = (EditText) findViewById(R.id.iptsourceipv6ET3);
    	sourceIPv6ET3.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET4 = (EditText) findViewById(R.id.iptsourceipv6ET4);
    	sourceIPv6ET4.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET5 = (EditText) findViewById(R.id.iptsourceipv6ET5);
    	sourceIPv6ET5.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET6 = (EditText) findViewById(R.id.iptsourceipv6ET6);
    	sourceIPv6ET6.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET7 = (EditText) findViewById(R.id.iptsourceipv6ET7);
    	sourceIPv6ET7.addTextChangedListener(ipv6Validation);
    	sourceIPv6ET8 = (EditText) findViewById(R.id.iptsourceipv6ET8);
    	sourceIPv6ET8.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET1 = (EditText) findViewById(R.id.iptdestinationipv6ET1);
    	destinationIPv6ET1.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET2 = (EditText) findViewById(R.id.iptdestinationipv6ET2);
    	destinationIPv6ET2.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET3 = (EditText) findViewById(R.id.iptdestinationipv6ET3);
    	destinationIPv6ET3.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET4 = (EditText) findViewById(R.id.iptdestinationipv6ET4);
    	destinationIPv6ET4.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET5 = (EditText) findViewById(R.id.iptdestinationipv6ET5);
    	destinationIPv6ET5.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET6 = (EditText) findViewById(R.id.iptdestinationipv6ET6);
    	destinationIPv6ET6.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET7 = (EditText) findViewById(R.id.iptdestinationipv6ET7);
    	destinationIPv6ET7.addTextChangedListener(ipv6Validation);
    	destinationIPv6ET8 = (EditText) findViewById(R.id.iptdestinationipv6ET8);
    	destinationIPv6ET8.addTextChangedListener(ipv6Validation);
    	ipv4Ipv6 = (RadioGroup) findViewById(R.id.iptip4ip6);
    	ipv4 = (RadioButton) findViewById(R.id.iptipv4);
    	ipv6 = (RadioButton) findViewById(R.id.iptipv6);
    	sourceIPv4RL = (RelativeLayout) findViewById(R.id.iptsourceipRL);
    	destinationIPv4RL = (RelativeLayout) findViewById(R.id.iptdestinationipRL);
    	sourceIPv6RL = (RelativeLayout) findViewById(R.id.iptsourceipv6RL);
    	destinationIPv6RL = (RelativeLayout) findViewById(R.id.iptdestinationipv6RL);
    	destinationIPHeader = (RelativeLayout) findViewById(R.id.iptdestinationipRLH);
    	sourceMaskPortHeader = (RelativeLayout) findViewById(R.id.iptsourcemaskportRLH);

    	destinationIPP = (LayoutParams) destinationIPHeader.getLayoutParams();
    	sourceMaskPortP = (LayoutParams) sourceMaskPortHeader.getLayoutParams();

    	allControlsForDisable = new View[]{tables, table, otherUp, otherTableChB, protocols, all, otherProtocol,
    			allChB, inInterface, inInterfaceET, outInterface, outInterfaceET, sourceIP, destinationIP,
    			sourceMaskET, sourcePortET, destinationMaskET, destinationPortET, targets, target, otherDown,
    			otherTargetChB, state, states, point1, point2, point3, point4, point5, point6,
    			doublePoint1, doublePoint2, doublePoint3, doublePoint4, doublePoint5, doublePoint6, doublePoint7,
    			doublePoint8, doublePoint9, doublePoint10, doublePoint11, doublePoint12, doublePoint13,
    			doublePoint14, sourceIPv6ET1, sourceIPv6ET2, sourceIPv6ET3, sourceIPv6ET4, sourceIPv6ET5,
    			sourceIPv6ET6, sourceIPv6ET7, sourceIPv6ET8, destinationIPv6ET1, destinationIPv6ET2,
    			destinationIPv6ET3, destinationIPv6ET4,	destinationIPv6ET5, destinationIPv6ET6,
    			destinationIPv6ET7, destinationIPv6ET8, sourceIPET1, sourceIPET2, sourceIPET3, sourceIPET4,
    			destinationIPET1, destinationIPET2, destinationIPET3, destinationIPET4, ipv4, ipv6,
    			otherProtocolChB};

    	newChainControls = new View[]{otherTableET};
    	policyControls = new View[]{tables, table, targets, target};

    	sourceIPv4Controls = new EditText[]{sourceIPET1, sourceIPET2, sourceIPET3, sourceIPET4};
    	destinationIPv4Controls = new EditText[]{destinationIPET1, destinationIPET2, destinationIPET3,
    			destinationIPET4};
    	sourceIPv6Controls = new EditText[]{sourceIPv6ET1, sourceIPv6ET2, sourceIPv6ET3, sourceIPv6ET4,
    			sourceIPv6ET5, sourceIPv6ET6, sourceIPv6ET7, sourceIPv6ET8};
    	destinationIPv6Controls = new EditText[]{destinationIPv6ET1, destinationIPv6ET2, destinationIPv6ET3,
    			destinationIPv6ET4, destinationIPv6ET5, destinationIPv6ET6, destinationIPv6ET7,
    			destinationIPv6ET8};

		setEnabledAndFocusable(otherTableET, false);
		setEnabledAndFocusable(otherTargetET, false);
		setEnabledAndFocusable(otherProtocolET, false);

		SharedPreferences preferences = getSharedPreferences("preferences", 0);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("type", "ipt");
		editor.commit();
    }

    @Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
