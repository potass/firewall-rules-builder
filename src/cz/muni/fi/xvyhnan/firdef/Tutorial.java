/**
 *
 */
package cz.muni.fi.xvyhnan.firdef;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.ViewFlipper;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.analytics.tracking.android.EasyTracker;

import cz.muni.fi.xvyhnan.components.CustomAlertDialogBuilder;

/**
 * Shows the tutorial.
 * @author Jan Vyhn�nek
 */
public class Tutorial extends SherlockActivity {
	private final String ERROR = "!!!";
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private ViewFlipper flipper = null;
	private Button next, prev;
	private Button selectedDBRuleBTN = null;
	private EditText sourceIPET1, sourceIPET3, sourceIPET4, destinationIPET1, destinationIPET2,
			destinationIPET4;
	private ImageView tutorial15, tutorial20, tutorial11;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.tutorial);
		setTitle(getString(R.string.activitytutorialtitle));

		this.flipper = (ViewFlipper) findViewById(R.id.tutorialflipper);

		next = (Button)this.findViewById(R.id.tutorialnext);
		next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(isLastDisplayed()) {
					Tutorial.this.setResult(Activity.RESULT_OK);
					Tutorial.this.finish();
				} else {
					flipper.showNext();
				}
			}
		});

		prev = (Button)this.findViewById(R.id.tutorialprev);
		prev.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(isFirstDisplayed()) {
					Tutorial.this.setResult(Activity.RESULT_CANCELED);
					Tutorial.this.finish();
				} else {
					flipper.showPrevious();
				}
			}
		});

		tutorial15 = (ImageView) findViewById(R.id.imgtut15);
        tutorial20 = (ImageView) findViewById(R.id.imgtut20);
        tutorial11 = (ImageView) findViewById(R.id.consoletut);

		if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			tutorial15.setImageDrawable(getResources().getDrawable(R.drawable.tut15_white));
			tutorial20.setImageDrawable(getResources().getDrawable(R.drawable.tut20_white));
			tutorial11.setImageDrawable(getResources().getDrawable(R.drawable.tut11_white));
		}

		sourceIPET1 = (EditText) findViewById(R.id.iptsourceipET1tut);
    	sourceIPET3 = (EditText) findViewById(R.id.iptsourceipET3tut);
    	sourceIPET4 = (EditText) findViewById(R.id.iptsourceipET4tut);
    	destinationIPET1 = (EditText) findViewById(R.id.iptdestinationipET1tut);
    	destinationIPET2 = (EditText) findViewById(R.id.iptdestinationipET2tut);
    	destinationIPET4 = (EditText) findViewById(R.id.iptdestinationipET4tut);
    	setError(sourceIPET1);
    	setError(sourceIPET3);
    	setError(sourceIPET4);
    	setError(destinationIPET1);
    	setError(destinationIPET2);
    	setError(destinationIPET4);
	}

	/**
	 * Determines whether the first view of tutorial is displayed.
	 * @return True if the first view is displayed, false otherwise.
	 */
	private boolean isFirstDisplayed() {
		return (flipper.getDisplayedChild() == 0);
	}

	/**
	 * Determines whether the last view of tutorial is displayed.
	 * @return True if the last view is displayed, false otherwise.
	 */
	private boolean isLastDisplayed() {
		return (flipper.getDisplayedChild() == flipper.getChildCount() - 1);
	}

    /**
	 * Enables error highlighting of control.
	 * @param control View which has an invalid value.
	 */
    public void setError(View control) {
    	EditText et = (EditText)control;
		SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		et.getBackground().setColorFilter(
				Integer.valueOf(colorPreferences.getString("errorscolor",
						DEFAULT_ERROR_COLOR)), PorterDuff.Mode.MULTIPLY);
		et.setTextColor(Integer.valueOf(colorPreferences.getString(
				"errorscolor", DEFAULT_ERROR_COLOR)));
		et.setText(ERROR);
    }

    /**
     * Shows database.
     * @param v Pressed button.
     */
    public void onShowDBClick(View v) {
		SharedPreferences colorPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		TableLayout tableLayout = new TableLayout(getApplicationContext());
		tableLayout.removeAllViews();
		ScrollView scrollView = new ScrollView(getApplicationContext());

		List<String> columns = new ArrayList<String>();
		columns.add("iptables -A INPUT");

		final int backgroundColor;
		if (colorPreferences.getString("apptheme", "Black").equals("Light")) {
			backgroundColor = Color.WHITE;
		} else {
			backgroundColor = Color.BLACK;
		}

		TableRow tableRow = new TableRow(getApplicationContext());
		tableRow.setGravity(17);
		tableRow.setBackgroundColor(backgroundColor);

		View divider = new View(getApplicationContext());
		divider.setBackgroundColor(Color.rgb(255, 172, 0));
		View divider2 = new View(getApplicationContext());
		divider2.setBackgroundColor(Color.rgb(255, 172, 0));

		for (String column : columns) {
			final Button columsView = new Button(getApplicationContext());

				columsView.setText(column);
				columsView.setTypeface(null, Typeface.BOLD);
				columsView.setTextColor(Color.rgb(255, 172, 0));
				columsView.setTextSize(16);
				columsView.setGravity(17);
				columsView.setBackgroundColor(backgroundColor);

				columsView.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						if (selectedDBRuleBTN != null) {
							selectedDBRuleBTN.setTextColor(Color.rgb(255, 172,
									0));
							selectedDBRuleBTN
									.setBackgroundColor(backgroundColor);
							((TableRow) selectedDBRuleBTN.getParent())
									.setBackgroundColor(backgroundColor);
						}

						Button selectedRow = (Button) v;
						if (selectedDBRuleBTN == selectedRow) {
							selectedDBRuleBTN = null;
							selectedRow.setTextColor(Color.rgb(255, 172, 0));
							selectedRow.setBackgroundColor(backgroundColor);
							((TableRow) selectedRow.getParent())
									.setBackgroundColor(backgroundColor);
						} else {
							selectedDBRuleBTN = selectedRow;
							selectedRow.setTextColor(backgroundColor);
							selectedRow.setBackgroundColor(Color.rgb(255, 172,
									0));
							((TableRow) selectedRow.getParent())
									.setBackgroundColor(Color.rgb(255, 172, 0));
						}
					}
				});

				tableRow.addView(columsView, LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
		}

		tableLayout.addView(divider, LayoutParams.FILL_PARENT, 2);
		tableLayout.addView(tableRow, LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		tableLayout.addView(divider2, LayoutParams.FILL_PARENT, 2);

		scrollView.addView(tableLayout);

		CustomAlertDialogBuilder dialog = new CustomAlertDialogBuilder(this);
		dialog.setIcon(android.R.drawable.ic_menu_save);
		dialog.setTitle("Stored rules");
		dialog.setView(scrollView);
		dialog.setPositiveButton("Select", null);
		dialog.setNeutralButton("Delete", null);
		dialog.setNegativeButton("Delete all", null);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

    /**
     * Shows edit dialog.
     * @param v Pressed button.
     */
    public void onShowEditClick(View v) {
    	final EditText editedCmd = new EditText(this);
    	final LinearLayout layout = new LinearLayout(this);
    	editedCmd.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    	editedCmd.setText("iptables -A INPUT");
    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    	params.setMargins(10, 0, 10, 0);
    	editedCmd.setLayoutParams(params);
    	layout.addView(editedCmd);

		new AlertDialog.Builder(this)
				.setMessage("Edit the generated command, but be carefull, it will not be validated!")
				.setView(layout).setPositiveButton("Save", null)
				.setNegativeButton("Back", null).show();
    }

	@Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
