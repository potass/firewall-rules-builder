/**
 *
 */
package cz.muni.fi.xvyhnan.firdef;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Color settings.
 * @author Jan Vyhn�nek
 */
public class ColorSettings extends SherlockPreferenceActivity {
	private final String DEFAULT_ERROR_COLOR = "2147418112";
	private final String DEFAULT_ERROR_COLOR_WHITE = "-16754812";
	private String theme = "";

	@Override
	public void onCreate(Bundle savedInstaceState) {
		final SharedPreferences colorPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		this.theme = colorPreferences.getString("apptheme", "Black");
    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
    		setTheme(R.style.HoloLight_Orange);
    	}

		super.onCreate(savedInstaceState);
		setTitle(getString(R.string.activitycolortitle));

		addPreferencesFromResource(R.xml.colorpreferences);

		Preference apptheme = findPreference("apptheme");
		apptheme.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				theme = colorPreferences.getString("apptheme", "Black");
				return true;
			}
		});

		apptheme.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				if(!((String)newValue).equals(theme)) {
			    	Editor editor = colorPreferences.edit();
			    	if(colorPreferences.getString("apptheme", "Black").equals("Light")) {
			    		editor.putString("errorscolor", DEFAULT_ERROR_COLOR);
			    	} else {
			    		editor.putString("errorscolor", DEFAULT_ERROR_COLOR_WHITE);
			    	}
			    	editor.commit();

		    		Intent intent = getIntent();
		        	finish();
		        	startActivity(intent);
		        	return true;
		    	}
		        return false;
		    }
		});
	}

	@Override
	public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance().activityStart(this);
	}

	@Override
	public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance().activityStop(this);
	}
}
